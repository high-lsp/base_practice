//挂载vue模块
var a1 = new Vue({
    el:'#app',
    components :{
        'demo':httpVueLoader('./views/demo.vue')
    }
})

var a2 = new Vue({
    el:'#app2',
    data:{
        message:"页面加载于 "+new Date().toLocaleString()
    },
    created:function () {
        //生命周期钩子的 this 上下文指向调用它的 Vue 实例
        console.log("msg is " + this.message);
    }
})

var app3 = new Vue({
    el: '#app-3',
    data: {
        seen: true
    }
})

var app4 = new Vue({
    el: '#app-4',
    data: {
        todos: [
            { text: '学习 JavaScript' },
            { text: '学习 Vue' },
            { text: '整个牛项目' }
        ]
    }
})

var app5 = new Vue({
    el: '#app-5',
    data: {
        message: 'Hello Vue.js!'
    },
    methods: {
        reverseMessage: function () {
            this.message = this.message.split('').reverse().join('')
        }
    }
})

var app6 = new Vue({
    el: '#app-6',
    data: {
        message: 'Hello Vue!'
    }
})