package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 14:44
 */
abstract class Shap {
    void draw() {
        System.out.println(this + ".draw()");
    }

    abstract public String toString();
}
