package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 16:00
 */
public class SweetShop {
    //使每种类型的创建由命令行参数控制


    public static void main(String[] args) {
        System.out.println("inside main");

        new Candy();
        System.out.println("After creating Candy");
        try {
            Class.forName("thinkInJava.chap14RTTI.Gum");
        } catch (ClassNotFoundException e) {
            System.out.println("Couldn't find Gum");
        }
        System.out.println("After class.forName(\"Gum\")");
        new Cookie();
        System.out.println("After creating Cookie");
    }
}
