package thinkInJava.chap14RTTI;

import java.util.Date;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 10:37
 */
public class SimpleProxy implements Interface {

    private Interface proxied;
    //度量方法调用的次数
    private static int doCount = 0;
    private static int sECount = 0;

    public SimpleProxy(Interface proxied) {
        this.proxied = proxied;
    }

    @Override
    public void doSomething() {
        long timeIn = new Date().getTime();
        System.out.println("SimpleProxy doSomething() " + doCount + ": " + timeIn + " msecs");
        System.out.println("on " + new Date());
        doCount++;
        proxied.doSomething();
        System.out.println("Call-return time =  " + ((new Date().getTime()) - timeIn) + " msecs");
    }

    @Override
    public void somethingElse(String arg) {
        long timeIn = new Date().getTime();
        System.out.println("SimpleProxy somethingElse() " + sECount + ": " + timeIn + " msecs");
        System.out.println("on " + new Date());
        sECount++;
        proxied.somethingElse(arg);
        System.out.println("Call-return time =  " + ((new Date().getTime()) - timeIn) + " msecs");
    }
}
