package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 10:27
 */
public interface Interface {
    void doSomething();

    void somethingElse(String arg);
}
