package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 14:47
 */
public class Triangle extends Shap {
    boolean flag = false;

    @Override
    public String toString() {
        return (flag ? "H" : "Unh") + "ighLinghted" + "Triangle";
    }
}
