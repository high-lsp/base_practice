package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 15:39
 */
public class Rhomboid extends Shap {

    @Override
    public String toString() {
        return "Rhomboid";
    }
}
