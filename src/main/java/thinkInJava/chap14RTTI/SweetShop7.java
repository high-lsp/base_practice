package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 16:15
 */
public class SweetShop7 {

    public static void main(String[] args) {
        System.out.println(args.length);
        if (args.length < 1) {
            System.out.println("Usage: sweetName");
            System.exit(0);
        }
        Class c = null;
        try {
            c = Class.forName(args[0]);
            System.out.println("Enjoy your " + c.getSimpleName());
        } catch (ClassNotFoundException e) {
            System.out.println("Couldn't find " + args[0]);
        }
    }
}
