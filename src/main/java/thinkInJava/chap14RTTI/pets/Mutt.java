package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:30
 */
public class Mutt extends Dog {
    public Mutt() {
    }

    public Mutt(String name) {
        super(name);
    }
}
