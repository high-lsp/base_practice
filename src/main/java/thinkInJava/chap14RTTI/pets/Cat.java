package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:30
 */
public class Cat extends Pet {
    public Cat() {
    }

    public Cat(String name) {
        super(name);
    }
}
