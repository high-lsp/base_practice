package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:30
 */
public class Pug extends Dog {
    public Pug() {
    }

    public Pug(String name) {
        super(name);
    }
}
