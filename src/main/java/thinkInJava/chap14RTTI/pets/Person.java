package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 8:51
 */
public class Person extends Individual {
    public Person(String name) {
        super(name);
    }
}
