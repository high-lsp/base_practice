package thinkInJava.chap14RTTI.pets;

import java.util.ArrayList;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 16:37
 */
public class Pets {
    public static final PetCreator creator = new LiteralPetCreator();

    public static Pet randomPet() {
        return creator.randomPet();
    }

    public static Pet[] createArray(int size) {
        return creator.createArray(size);
    }

    public static ArrayList<Pet> arrayList(int size) {
        return creator.arrayList(size);
    }
}
