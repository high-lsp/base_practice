package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:32
 */
public class Cymric extends Manx {
    public Cymric() {
    }

    public Cymric(String name) {
        super(name);
    }
}
