package thinkInJava.chap14RTTI.pets;

import java.util.*;

/**
 * @description: 随机创建不同类型的宠物，为了方便起见，还提供了创建宠物的数组和 List
 * @author: Lisp
 * @time: 2020/9/18 15:35
 */
public abstract class PetCreator {
    private Random rand = new Random(47);

    //the list of the different types of pet to create
    public abstract List<Class<? extends Pet>> types();

    //create a random pet
    public Pet randomPet() {
        int n = rand.nextInt(types().size());
        try {
            return types().get(n).newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public Pet[] createArray(int size) {
        Pet[] result = new Pet[size];
        for (int i = 0; i < size; i++) {
            result[i] = randomPet();
        }
        return result;
    }

    public ArrayList<Pet> arrayList(int size) {
//        return Arrays.asList(createArray(size));
        ArrayList<Pet> result = new ArrayList<>();
        Collections.addAll(result, createArray(size));
        return result;
    }
}
