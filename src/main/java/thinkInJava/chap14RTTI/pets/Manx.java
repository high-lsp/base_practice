package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:32
 */
public class Manx extends Cat {
    public Manx() {
    }

    public Manx(String name) {
        super(name);
    }
}
