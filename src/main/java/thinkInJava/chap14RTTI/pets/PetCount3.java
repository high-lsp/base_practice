package thinkInJava.chap14RTTI.pets;

import java.util.LinkedHashMap;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 16:42
 */
public class PetCount3 {
    static class PetCounter extends LinkedHashMap<Class<? extends Pet>, Integer> {
        public PetCounter() {
            super();
        }
    }
}
