package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:32
 */
public class Rodent extends Pet {
    public Rodent() {
    }

    public Rodent(String name) {
        super(name);
    }
}
