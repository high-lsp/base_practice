package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 8:55
 */
public class Individual {
    String name;
    int id;

    public Individual() {

    }

    public Individual(String name) {
        this.name = name;
    }

    public Individual(String name, int id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
