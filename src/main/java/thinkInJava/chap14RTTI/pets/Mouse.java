package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:33
 */
public class Mouse extends Rodent {
    public Mouse() {
    }

    public Mouse(String name) {
        super(name);
    }
}
