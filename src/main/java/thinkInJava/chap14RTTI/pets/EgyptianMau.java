package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:31
 */
public class EgyptianMau extends Cat {
    public EgyptianMau() {
    }

    public EgyptianMau(String name) {
        super(name);
    }
}
