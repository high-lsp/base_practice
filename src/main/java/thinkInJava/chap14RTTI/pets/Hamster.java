package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:34
 */
public class Hamster extends Rodent {
    public Hamster() {
    }

    public Hamster(String name) {
        super(name);
    }
}
