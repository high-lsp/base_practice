package thinkInJava.chap14RTTI.pets;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:52
 */
public class ForNameCreator extends PetCreator {
    private static List<Class<? extends Pet>> types = new ArrayList<Class<? extends Pet>>();
    private static String[] typeNames = {
            "thinkInJava.chap14RTTI.pets.Mutt",
            "thinkInJava.chap14RTTI.pets.Pug",
            "thinkInJava.chap14RTTI.pets.EgyptianMau",
            "thinkInJava.chap14RTTI.pets.Manx",
            "thinkInJava.chap14RTTI.pets.Cymric",
            "thinkInJava.chap14RTTI.pets.Rat",
            "thinkInJava.chap14RTTI.pets.Mouse",
            "thinkInJava.chap14RTTI.pets.Hamster",
    };

    private static void loader() {
        try {
            for (String name : typeNames) {
                types.add((Class<? extends Pet>) Class.forName(name));
            }
            System.out.println(types.size() + " 个");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        loader();
    }

    @Override
    public List<Class<? extends Pet>> types() {
        return types;
    }
}
