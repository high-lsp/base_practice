package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 15:29
 */
public class Dog extends Pet {
    public Dog() {
    }

    public Dog(String name) {
        super(name);
    }
}
