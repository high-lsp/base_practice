package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 17:17
 */
public class PetCount4 {
    public static void main(String[] args) {
        TypeCounter counter = new TypeCounter(Pet.class);
        for (Pet pet : Pets.arrayList(20)) {
            System.out.println(pet.getClass().getSimpleName() + "");
            counter.count(pet);
        }
        System.out.println("------------");

        System.out.println(counter);
    }
}
