package thinkInJava.chap14RTTI.pets;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 8:58
 */
public class Pet extends Individual {

    public Pet() {
        super();
    }

    public Pet(String name) {
        super(name);
    }
}
