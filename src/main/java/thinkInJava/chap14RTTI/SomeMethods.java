package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 14:11
 */
public interface SomeMethods {
    void boring1();

    void boring2();

    void boring3();

    void interesting(String arg);
}
