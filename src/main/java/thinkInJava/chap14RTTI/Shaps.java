package thinkInJava.chap14RTTI;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 14:48
 */
public class Shaps {

    static void roate(Shap shap) {
        if (shap instanceof Circle) {
            System.out.println("shap is circle ,can't runing");
        }
    }

    //标记
    static void setFlag(Shap s) {
        if (s instanceof Triangle) {
            ((Triangle) s).flag = true;
        }
    }

    public static void main(String[] args) {
        List<Shap> shapeList = Arrays.asList(new Circle(), new Square(), new Triangle(), new Rhomboid());
        for (Shap shap : shapeList) {
            setFlag(shap);
            shap.draw();
        }
        Rhomboid rhomboid = new Rhomboid();
        ((Shap) rhomboid).draw();
        Shap s = (Shap) rhomboid;
        //在向下转型之前先运用 instanceof 检查类型
        if (s instanceof Circle) {
            ((Circle) s).draw();
        } else if (!(s instanceof Circle)) {
            System.out.println("(Shap)r is not a circle");
        }
    }
}
