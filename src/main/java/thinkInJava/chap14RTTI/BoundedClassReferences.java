package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 8:31
 */
public class BoundedClassReferences {
    public static void main(String[] args) {
        Class<? extends Number> bounded = int.class;
        bounded = double.class;
        bounded = Number.class;

    }

}
