package thinkInJava.chap14RTTI;

import java.lang.reflect.Proxy;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 14:13
 */
public class Implementation implements SomeMethods {
    @Override
    public void boring1() {
        System.out.println("boring1");
    }

    @Override
    public void boring2() {
        System.out.println("boring2");
    }

    @Override
    public void boring3() {
        System.out.println("boring3");
    }

    @Override
    public void interesting(String arg) {
        System.out.println("interesting " + arg);
    }


    public static void main(String[] args) {
        SomeMethods proxied = (SomeMethods) Proxy.newProxyInstance(SomeMethods.class.getClassLoader(),
                new Class[]{SomeMethods.class}, new MethodSelector(new Implementation()));
        proxied.boring1();
        proxied.boring2();
        proxied.boring3();
        proxied.interesting("hello");
    }
}
