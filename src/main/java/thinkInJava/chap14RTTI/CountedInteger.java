package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/18 8:33
 */
public class CountedInteger {
    private static long counter;
    private final long id = counter++;

    public String toString() {
        return Long.toString(id);
    }
}
