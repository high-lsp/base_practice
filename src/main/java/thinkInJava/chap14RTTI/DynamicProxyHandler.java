package thinkInJava.chap14RTTI;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @description: Java 动态代理：可以动态的创建代理并动态的处理对所代理方法的调用。
 * 在动态代理上所做的所有调用都会被重定向到单一的  调用处理器  上，
 * 它的工作是揭示调用的类型并确定相应的对策
 * @author: Lisp
 * @time: 2020/9/18 11:02
 */
public class DynamicProxyHandler implements InvocationHandler {

    private Object proxied;

    public DynamicProxyHandler(Object proxied) {
        this.proxied = proxied;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("******* proxy: " + proxy.getClass() + ",method: " + method + ",args: " + args);
        if (args != null) {
            for (Object arg : args) {
                System.out.println("  " + arg);
            }
        }
        return method.invoke(proxied, args);
    }
}
