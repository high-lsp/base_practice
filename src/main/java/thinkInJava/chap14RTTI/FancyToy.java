package thinkInJava.chap14RTTI;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 15:12
 */
public class FancyToy extends Toy implements HasBatteries, Waterproof, Shoots, Files {

    FancyToy() {
        super(1);
    }
}
