package thinkInJava.chap12Exception;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 9:24
 */
public class Switch {

    private boolean state = false;

    public boolean read() {
        return state;
    }

    public void on() {
        state = true;
        System.out.println(this);
    }

    public void off() {
        state = false;
        System.out.println(this);
    }

    public String toString() {
        return state ? "on" : "off";
    }
}
