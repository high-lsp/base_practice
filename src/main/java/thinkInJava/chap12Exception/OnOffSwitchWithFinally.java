package thinkInJava.chap12Exception;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 9:34
 */
public class OnOffSwitchWithFinally {
    static Switch sw = new Switch();

    public static void main(String[] args) {
        try {
            sw.on();
            OnOffSwitch.f();
        } catch (OnOffException1 onOffException1) {
            System.out.println("OnOffException1");
        } catch (OnOffException2 onOffException2) {
            System.out.println("OnOffException2");
        } finally {
            sw.off();
        }
    }
}
