package thinkInJava.chap12Exception;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 9:15
 */
public class NeverCaught {
    static void f() {
        throw new RuntimeException("from f()");
    }

    static void g() {
        f();
    }

    public static void main(String[] args) {
        g();
    }


}
