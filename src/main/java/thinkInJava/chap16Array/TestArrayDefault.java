package thinkInJava.chap16Array;

import java.util.Arrays;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/22 15:50
 */
public class TestArrayDefault {

    public static void main(String[] args) {
//        char[] c = new char[5];
//        for (int i = 0; i < 5; i++) {
//            System.out.println(c[i]);
//        }
        int[][] a = {{1, 2, 3}, {4, 5, 6}};
        System.out.println(Arrays.deepToString(a));
    }
}
