package thinkInJava.chap15TypeParam;

/**
 * @description: 通过泛型实现内部类和匿名内部类
 * @author: Lisp
 * @time: 2020/9/29 14:32
 */
public class Customer {
    private static long counter = 1;
    private final long id = counter++;

    private Customer() {

    }

    public String toString() {
        return "Customer " + id;
    }

    public static Generator<Customer> generator() {
        return new Generator<Customer>() {
            @Override
            public Customer next() {
                return new Customer();
            }
        };
    }

}
