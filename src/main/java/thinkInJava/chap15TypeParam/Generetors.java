package thinkInJava.chap15TypeParam;

import com.lsp.java.TypeParam.generics.Coffee;
import com.lsp.java.TypeParam.generics.CoffeeGenerator;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/28 9:49
 */
public class Generetors {

    public static <T> Collection<T> fill(Collection<T> coll, Generator<T> gen, int n) {
        for (int i = 0; i < n; i++) {
            coll.add(gen.next());
        }
        return coll;
    }
}
