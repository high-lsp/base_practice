package thinkInJava.chap15TypeParam;

import java.util.*;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/23 15:23
 */
public class New {
    public static <K, V> Map<K, V> map() {
        return new HashMap<K, V>();
    }

    public static <T> List<T> list() {
        return new ArrayList<>();
    }

    public static <T> LinkedList<T> lList() {
        return new LinkedList<>();
    }

    private static <T> Set<T> set() {
        return new HashSet<>();
    }

    public static <T> Queue<T> queue() {
        return new LinkedList<>();
    }

    public static void main(String[] args) {
        Map<String, List<String>> sls = New.map();
        List<Object> ls = New.list();
        LinkedList<Object> lls = New.lList();
        Set<Object> ss = New.set();
        Queue<Object> qs = New.queue();
    }


}
