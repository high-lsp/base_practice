package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/21 10:12
 */
public class FiveTuple<A, B, C, D, E> extends FourTuple<A, B, C, D> {
    private final E fifth;

    public FiveTuple(A first, B second, C c, D fourth, E fifth) {
        super(first, second, c, fourth);
        this.fifth = fifth;
    }

    @Override
    public String toString() {
        return "FiveTuple{" +
                "fifth=" + fifth +
                ", first=" + first +
                ", second=" + second +
                '}';
    }
}
