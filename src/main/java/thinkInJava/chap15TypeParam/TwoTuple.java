package thinkInJava.chap15TypeParam;

/**
 * @description: 泛型：适用于许多许多的类型，泛型实现了参数化类型的概念。
 * 元组：将一组对象直接打包存储于其中的一个单一对象。
 * 这个容器对象允许读取其中的元素，但是不允许向其中存放新的对象
 * @author: Lisp
 * @time: 2020/9/21 9:58
 */
public class TwoTuple<A, B> {
    public final A first;
    public final B second;

    public TwoTuple(A first, B second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "TwoTuple{" +
                "first=" + first +
                ", second=" + second +
                '}';
    }
}
