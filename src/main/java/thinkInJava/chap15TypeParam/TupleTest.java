package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/21 10:49
 */
public class TupleTest {

    static TwoTuple<String, Integer> f() {
        return new TwoTuple<String, Integer>("hi", 47);
    }

    static ThreeTuple<Amphibian, String, Integer> g() {
        return new ThreeTuple<Amphibian, String, Integer>(new Amphibian(), "hi", 47);
    }

    static FourTuple<Vehicle, Amphibian, String, Integer> h() {
        return new FourTuple<>(new Vehicle(), new Amphibian(), "hi", 47);
    }

    static FiveTuple<Vehicle, Amphibian, String, Integer, Double> k() {
        return new FiveTuple<>(new Vehicle(), new Amphibian(), "hi", 47, 11.1);
    }

    public static void main(String[] args) {
        TwoTuple<String, Integer> ttsi = f();
//        ttsi.first = "thre"; will compile failuer
        System.out.println(ttsi);
        System.out.println(g());
        System.out.println(h());
        System.out.println(k().first);
    }
}
