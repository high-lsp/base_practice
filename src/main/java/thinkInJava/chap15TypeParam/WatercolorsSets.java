package thinkInJava.chap15TypeParam;

import java.util.EnumSet;
import java.util.Set;

/**
 * @description: EnumSet.range() 方法生成set
 * @author: Lisp
 * @time: 2020/9/28 11:01
 */
public class WatercolorsSets {
    public static void main(String[] args) {
        Set<Watercolors> set1 = EnumSet.range(Watercolors.BRILLIANT_RED, Watercolors.VIRIDIAN_HUE);
        Set<Watercolors> set2 = EnumSet.range(Watercolors.CERULEAN_BLUE_HUE, Watercolors.BURNT_UMBER);
        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);
        System.out.println("union(set1,set2)： " + SetsPractice.union(set1, set2));

    }
}
