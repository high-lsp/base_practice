package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/29 14:40
 */
public class Teller {
    private static long counter = 1;
    private final long id = counter++;

    private Teller() {

    }

    public String toString() {
        return "Teller: " + id;
    }

    public static Generator<Teller> generator() {
        return new Generator<Teller>() {
            @Override
            public Teller next() {
                return new Teller();
            }
        };
    }
}
