package thinkInJava.chap15TypeParam;

import java.util.*;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/29 14:41
 */
public class BankTeller {
    public static void serve(Teller t, Customer c) {
        System.out.println(t + " serves " + c);
    }

    public static void main(String[] args) {
        Random rand = new Random(47);
        Queue<Customer> line = new LinkedList<>();
        Generetors.fill(line, Customer.generator(), 15);
        List<Teller> tellers = new ArrayList<>();
        Generetors.fill(tellers, Teller.generator(), 4);
        for (Customer c : line) {
            serve(tellers.get(rand.nextInt(tellers.size())), c);
        }
    }
}
