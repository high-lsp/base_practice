package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/21 10:07
 */
public class ThreeTuple<A, B, C> extends TwoTuple<A, B> {
    private final C third;

    public ThreeTuple(A first, B second, C c) {
        super(first, second);
        this.third = c;
    }

    @Override
    public String toString() {
        return "ThreeTuple{" +
                "third=" + third +
                ", first=" + first +
                ", second=" + second +
                '}';
    }
}
