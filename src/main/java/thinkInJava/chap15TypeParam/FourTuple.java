package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/21 10:10
 */
public class FourTuple<A, B, C, D> extends ThreeTuple<A, B, C> {

    private final D fourth;

    public FourTuple(A first, B second, C c, D fourth) {
        super(first, second, c);
        this.fourth = fourth;
    }

    @Override
    public String toString() {
        return "FourTuple{" +
                "fourth=" + fourth +
                ", first=" + first +
                ", second=" + second +
                '}';
    }
}
