package thinkInJava.chap15TypeParam;

/**
 * @description: 生成器接口
 * @author: Lisp
 * @time: 2020/9/23 10:48
 */
public interface Generator<T> {
    //用于产生新的对象
    T next();
}
