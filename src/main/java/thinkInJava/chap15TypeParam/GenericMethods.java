package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/23 14:51
 */
public class GenericMethods {
    public <T> void f(T... x) {
        for (T v : x) {
            System.out.println(v.getClass().getSimpleName());
        }
    }

    public static void main(String[] args) {
        GenericMethods gm = new GenericMethods();
        gm.f("", 1, 1.0, 1.0F);
//        gm.f(1);
//        gm.f(1.0);
//        gm.f(1.0F);
//        gm.f("c");
//        gm.f(gm);

    }
}
