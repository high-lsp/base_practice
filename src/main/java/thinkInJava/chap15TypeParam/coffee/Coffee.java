package thinkInJava.chap15TypeParam.coffee;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/23 10:49
 */
public class Coffee {
    private static long counter = 0;
    private final long id = counter++;

    public String toString() {
        return getClass().getSimpleName() + " " + id;
    }
}
