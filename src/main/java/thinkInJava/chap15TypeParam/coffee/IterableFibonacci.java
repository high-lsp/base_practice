package thinkInJava.chap15TypeParam.coffee;

import java.util.Iterator;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/23 14:19
 */
public class IterableFibonacci extends Fibonacci implements Iterable<Integer> {

    private int n;

    public IterableFibonacci(int count) {
        this.n = count;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {
            @Override
            public boolean hasNext() {
                return n > 0;
            }

            @Override
            public Integer next() {
                n--;
                return IterableFibonacci.this.next();
            }
        };
    }

    public static void main(String[] args) {
        IterableFibonacci aa = new IterableFibonacci(3);
        System.out.println(aa.toString());
        for (int i : new IterableFibonacci(18)) {
            System.out.print(i + " ");
        }
    }
}
