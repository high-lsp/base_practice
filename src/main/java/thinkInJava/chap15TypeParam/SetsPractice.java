package thinkInJava.chap15TypeParam;

import java.util.HashSet;
import java.util.Set;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/28 10:41
 */
public class SetsPractice {
    //并集
    public static <T> Set<T> union(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.addAll(b);
        return result;
    }

    //交集
    public static <T> Set<T> intersecton(Set<T> a, Set<T> b) {
        Set<T> result = new HashSet<>(a);
        result.retainAll(b);
        return result;
    }

    //差集
    public static <T> Set<T> difference(Set<T> superset, Set<T> subset) {
        Set<T> result = new HashSet<>(superset);
        result.removeAll(subset);
        return result;
    }

    //返回交集之外的所有元素
    public static <T> Set<T> complement(Set<T> a, Set<T> b) {
        return difference(union(a, b), intersecton(a, b));
    }
}
