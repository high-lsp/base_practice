package thinkInJava.chap15TypeParam;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/28 10:11
 */
public class CountedObject {
    private static long counter = 0;
    private final long id = counter++;

    public long id() {
        return id;
    }

    public String toString() {
        return "CountedObject " + id;
    }

    public static void main(String[] args) {
        Generator<CountedObject> countedObjectGenerator = BasicGenerator.create(CountedObject.class);
        for (int i = 0; i < 5; i++) {
            System.out.println(countedObjectGenerator.next());
        }
    }
}
