package thinkInJava.chap13String;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 10:59
 */
public class Concatenation {

    public static void main(String[] args) {
        String mango = "mango";
        String s = "abc" + mango + "def" + 47;
        System.out.println(s);
    }
}
