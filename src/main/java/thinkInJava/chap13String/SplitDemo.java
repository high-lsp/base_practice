package thinkInJava.chap13String;

import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 9:48
 */
public class SplitDemo {
    public static void main(String[] args) {
//        String input = "This!!unusual use!!of exclamation!!points";
//        System.out.println(Arrays.toString(input.split("!!")));
//        System.out.println(Arrays.toString(input.split("!!",3)));
//        System.out.println(Arrays.toString(Pattern.compile("!!").split(input)));
//        //限制分割字符串的数量
//        System.out.println(Arrays.toString(Pattern.compile("!!").split(input, 3)));
    }
}
