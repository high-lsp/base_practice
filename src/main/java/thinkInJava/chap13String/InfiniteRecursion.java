package thinkInJava.chap13String;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 14:30
 */
public class InfiniteRecursion {
    @Override
    public String toString() {
        return "InfiniteRecursion address " + super.toString();
    }

    public static void main(String[] args) {
        List<InfiniteRecursion> v = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            v.add(new InfiniteRecursion());
        }
        System.out.println(v);
    }
}
