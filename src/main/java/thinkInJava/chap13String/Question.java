package thinkInJava.chap13String;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 17:19
 */
public class Question {

    public static void matchQ10(String[] regexs) {
        String value = "Java now has regular expressions";
        for (String regex : regexs) {
//            Matcher matcher = Pattern.compile(regex).matcher(value);
//            boolean r = matcher.find();
//            System.out.println(regex + "  compile result is : "+r+": m.group():");
        }
    }

    public static void main(String[] args) {
        String[] regexs = {"^Java", "\\Breg.*", "n.w\\s+h(a|i)s", "s?", "s*", "s+", "s{4}", "s{1}.", "s{0,3}", "."};
        matchQ10(regexs);
    }
}
