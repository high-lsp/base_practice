package thinkInJava.chap13String;

/**
 * @description: 正则表达式：字符、字符类、逻辑操作符、边界匹配符
 * @author: Lisp
 * @time: 2020/9/16 16:32
 */
public class Rudolph {


    public static void main(String[] args) {
        for (String pattern : new String[]{"Rudolph", "[rR]udolph", "[rR][aeiou][a-z]ol.*", "R.*"}) {
//            System.out.println("Rudolph".matches(pattern));
        }
    }
}
