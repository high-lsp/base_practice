package thinkInJava.chap13String;

import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/17 9:06
 */
public class Groups {

    static public final String POEM =
            "Twas brillig,and the slithy toves\n" +
                    "Did gyre and gimble in the wabe.\n" +
                    "All mimsy were the borogoves,\n" +
                    "And the mome raths outgrabe.\n\n" +
                    "Beware the Jabberwock,my son,\n" +
                    "The jaws that bite,the claws that catch.\n" +
                    "Beware the Jubjub bird,and shun\n" +
                    "The frumious Bandersnatch.";

    public static void main(String[] args) {
        //不以大写字母开头的词，不重复计算其个数
//        String regex = "(^[a-z]|\\s+[a-z])\\w+";
        String regex = "(^\\s[a-z])\\w+";
//        Matcher matcher = Pattern.compile("(?m)(\\S+)\\s+((\\S+)\\s+(\\S+))$").matcher(POEM);
//        Matcher matcher = Pattern.compile(regex).matcher(POEM);
//        Set<String> words = new TreeSet<>();
//        while (matcher.find()) {
//            for (int j = 0; j < matcher.groupCount(); j++) {
//                System.out.println("[" + matcher.group(j) + "]");
//            }
//            System.out.println("----------");
//            words.add(matcher.group());
//        }
//        System.out.println(words);
    }


}
