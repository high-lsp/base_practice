package thinkInJava.chap13String;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/9/16 14:12
 */
public class WhitherStringBuilder {

    public String implicit(String[] fields) {
        String result = "";
        for (int i = 0; i < fields.length; i++) {
            result += fields[i];
        }
        return result;
    }

    public String explicit(String[] fields) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < fields.length; i++) {
            result.append(fields[i]);
        }
        return result.toString();
    }

    public static void main(String[] args) {
        System.out.println(1);
    }
}
