package com.lsp.java.jfreeCharts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleInsets;

import java.awt.*;
import java.io.UnsupportedEncodingException;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/1/11 20:04
 */
public class TimeChart {
    public static void main(String[] args) throws UnsupportedEncodingException {

        // 创建主题样式
        StandardChartTheme mChartTheme = new StandardChartTheme("CN");
        // 设置标题字体
        mChartTheme.setExtraLargeFont(new Font(null, Font.BOLD, 20));
        // 设置轴向字体
        mChartTheme.setLargeFont(new Font(null, Font.CENTER_BASELINE, 15));
        // 设置图例字体
        mChartTheme.setRegularFont(new Font(null, Font.CENTER_BASELINE, 15));
        // 应用主题样式
        ChartFactory.setChartTheme(mChartTheme);

        CategoryDataset mDataset = GetDataset();
        JFreeChart chart = ChartFactory.createLineChart(
                "折线图",//图名字
                "",//横坐标
                "",//纵坐标
                mDataset,//数据集
                PlotOrientation.VERTICAL,
                true, // 显示图例
                true, // 采用标准生成器
                false);// 是否生成超链接

        chart.setBackgroundPaint(Color.white);
//        chart.getTitle().setFont(new Font("宋体", Font.BOLD, 180));
//        // 处理子标题乱码
//        chart.getLegend().setItemFont(new Font("宋体", Font.BOLD, 15));
        // 获取图表区域对象
        CategoryPlot categoryPlot = (CategoryPlot) chart.getPlot();
        //设置网格背景颜色
        categoryPlot.setBackgroundPaint(Color.white);
        //是否显示边界线
        categoryPlot.setOutlineVisible(false);

        //背景底部横实线
        categoryPlot.setRangeGridlinePaint(Color.black);
        categoryPlot.setDomainGridlineStroke(new BasicStroke(0.1f));
        categoryPlot.setRangeGridlineStroke(new BasicStroke(0.1f));

        //设置步数
        IntervalCategoryAxis axis = new IntervalCategoryAxis(1);
        categoryPlot.setDomainAxis(axis);

        // 获取Y轴的对象
        NumberAxis numberAxis = (NumberAxis) categoryPlot.getRangeAxis();
        numberAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());

        //设置折线形状
        //获得renderer 注意这里是下嗍造型到lineandshaperenderer！！
        LineAndShapeRenderer lineandshaperenderer = (LineAndShapeRenderer) categoryPlot.getRenderer();

        lineandshaperenderer.setSeriesStroke(0, new BasicStroke(3.5F));
        lineandshaperenderer.setSeriesPaint(0, new Color(255, 0, 0));

        lineandshaperenderer.setSeriesStroke(1, new BasicStroke(2.0F));
        lineandshaperenderer.setSeriesPaint(1, new Color(0, 128, 0));

        lineandshaperenderer.setSeriesStroke(2, new BasicStroke(2.0F));
        lineandshaperenderer.setSeriesPaint(2, new Color(0, 0, 255));


        /* 8、生成相应的图片 byte[] bytes = ChartUtilities.encodeAsPNG(chart.createBufferedImage(800, 600));*/
//        File file = new File("E:\\LineChart.JPEG");
//        try {
//            ChartUtilities.saveChartAsJPEG(file, chart, 800, 600);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        ChartFrame mChartFrame = new ChartFrame("折线图", chart);
        mChartFrame.pack();
        mChartFrame.setVisible(true);

    }

    public static CategoryDataset GetDataset() throws UnsupportedEncodingException {
        DefaultCategoryDataset mDataset = new DefaultCategoryDataset();
        mDataset.addValue(0, "当前时间", "2013");
        mDataset.addValue(0, "当前时间", "2014");
        mDataset.addValue(0, "当前时间", "2015");
//        mDataset.addValue(6, "当前时间", "2016");
//        mDataset.addValue(5, "当前时间", "2017");
//        mDataset.addValue(12, "当前时间", "2018");
        mDataset.addValue(0, "昨天", "2013");
        mDataset.addValue(0, "昨天", "2014");
        mDataset.addValue(0, "昨天", "2015");
//        mDataset.addValue(9, "昨天", "2016");
//        mDataset.addValue(5, "昨天", "2017");
//        mDataset.addValue(7, "昨天", "2018");
        mDataset.addValue(0, "前天", "2013");
        mDataset.addValue(0, "前天", "2014");
        mDataset.addValue(0, "前天", "2015");
//        mDataset.addValue(19, "前天", "2016");
//        mDataset.addValue(15, "前天", "2017");
//        mDataset.addValue(17, "前天", "2018");
        return mDataset;
    }
}
