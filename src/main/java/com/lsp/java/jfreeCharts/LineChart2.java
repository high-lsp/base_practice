package com.lsp.java.jfreeCharts;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;

import java.awt.*;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/1/13 11:02
 */
public class LineChart2 {

    public static void main(String[] args) {


        // 添加数据集
        DefaultCategoryDataset dataSet = new DefaultCategoryDataset();
        dataSet.addValue(3542, "2020年", "第一季度");// Y轴-图例-X轴
        dataSet.addValue(3692, "2020年", "第二季度");// Y轴-图例-X轴
        dataSet.addValue(8542, "2020年", "第三季度");// Y轴-图例-X轴
        dataSet.addValue(5742, "2020年", "第四季度");// Y轴-图例-X轴

        dataSet.addValue(1242, "2021年", "第一季度");// Y轴-图例-X轴
        dataSet.addValue(2612, "2021年", "第二季度");// Y轴-图例-X轴
        dataSet.addValue(1942, "2021年", "第三季度");// Y轴-图例-X轴
        dataSet.addValue(4612, "2021年", "第四季度");// Y轴-图例-X轴


        // 主题样式设置
        StandardChartTheme standardChartTheme = new StandardChartTheme("CN"); // 创建主题样式
        standardChartTheme.setExtraLargeFont(new Font("宋体", Font.BOLD, 64)); // 设置标题字体
        standardChartTheme.setRegularFont(new Font("宋体", Font.BOLD, 32)); // 设置图例的字体
        standardChartTheme.setLargeFont(new Font("宋体", Font.BOLD, 20)); // 设置轴向的字体
        standardChartTheme.setChartBackgroundPaint(Color.WHITE);// 设置主题背景色
        ChartFactory.setChartTheme(standardChartTheme);// 应用主题样式


        // 定义图表对象
        JFreeChart chart = ChartFactory.createLineChart(
                // 图表的标题-横轴标题-纵轴标题-数据集
                "JFreeChart折线图", "销售额", "季度", dataSet,
                // 图表方向-图例-工具提示-超链接
                PlotOrientation.VERTICAL, true, false, false);
        // chart.setTitle(new TextTitle(title[0], new Font("宋书", Font.BOLD, 64)));// 重新设置标题
        // chart.removeLegend();// 是否移除图例
        CategoryPlot plot = (CategoryPlot) chart.getPlot(); // 获得图表显示对象
        plot.setOutlineVisible(false);// 是否显示外边框
        plot.setOutlinePaint(Color.WHITE);// 外边框颜色
        // plot.setOutlineStroke(new BasicStroke(2.f));// 外边框框线粗细
        plot.setBackgroundPaint(Color.WHITE);// 白色背景
        plot.setNoDataMessage("无图表数据");// 无数据提示
        plot.setNoDataMessageFont(new Font("宋体", Font.BOLD, 32));// 提示字体
        plot.setNoDataMessagePaint(Color.RED);// 提示字体颜色


        // 图例
        LegendTitle legend = chart.getLegend();// 图例对象
        legend.setPosition(RectangleEdge.BOTTOM);// 图例所在位置(上、下、左、右)
        legend.setVisible(true);// 是否显示图例
        legend.setBorder(2, 0, 0, 2); // 图例上下左右边框粗细
        legend.setItemFont(new Font("宋体", Font.BOLD, 32));// 图例大小


        // 网格线
        plot.setDomainGridlinePaint(Color.BLUE);
        plot.setDomainGridlinesVisible(true);// 竖线
        plot.setRangeGridlinePaint(Color.BLACK);
        plot.setRangeGridlinesVisible(true);// 横线


        // 横坐标
        CategoryAxis xAxis = plot.getDomainAxis();
        xAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 25));// 设置X轴值字体
        xAxis.setLabelFont(new Font("宋书", Font.BOLD, 32));// 设置X轴标签字体
        xAxis.setAxisLineStroke(new BasicStroke(1f)); // 设置X轴线粗细
        xAxis.setCategoryLabelPositions(CategoryLabelPositions.UP_45);// 倾斜
        xAxis.setAxisLinePaint(Color.BLACK);// 轴线颜色
        xAxis.setUpperMargin(0.18D);// 右边距
        xAxis.setLowerMargin(0.1D);// 左边距


        // 纵坐标
        ValueAxis yAxis = plot.getRangeAxis();
        yAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 25));// 设置y轴字体
        yAxis.setLabelFont(new Font("宋体", Font.BOLD, 32));// 设置X轴标签字体
        yAxis.setAxisLineStroke(new BasicStroke(1f)); // 设置y轴线粗细
        yAxis.setAxisLinePaint(Color.BLACK);// 轴线颜色
        yAxis.setUpperMargin(0.18D);// 上边距
        yAxis.setLowerMargin(0.1D);// 下边距


        LineAndShapeRenderer r1 = new LineAndShapeRenderer();
        // 数据标签
        r1.setBaseItemLabelsVisible(true);// 是否显示数据标签
        r1.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());// 数据标签格式
        r1.setSeriesPaint(0, Color.BLUE);
        r1.setBaseShapesFilled(Boolean.TRUE); // 在数据点显示实心的小图标
        r1.setBaseShapesVisible(true); // 设置显示小图标
        r1.setBaseItemLabelFont(new Font("Dialog", Font.BOLD, 12));// 设置数字的字体大小
        r1.setBaseStroke(new BasicStroke(4f));
        // 线条颜色
        r1.setSeriesPaint(0, new Color(204, 232, 207));
        r1.setSeriesPaint(1, new Color(0x00, 0xff, 0xff));
        r1.setSeriesShapesVisible(0, false);
        r1.setSeriesShapesVisible(1, false);
        plot.setDataset(dataSet);
        // 应用以上设置
        plot.setRenderer(r1);

        // 条形图间距设置
        // BarRenderer r = (BarRenderer) plot.getRenderer();
        // renderer.setItemMargin(0.01);


        // 显示图表
        ChartFrame chartFrame = new ChartFrame("Test", chart);
        chartFrame.pack();
        chartFrame.setVisible(true);
    }
}
