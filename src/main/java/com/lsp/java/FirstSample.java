package com.lsp.java;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * @Auther: Lisp
 * @Date: 2018/12/5 17:00
 * @Description: 这是一个简单的类
 */
public class FirstSample {
    public static void main(String[] args) {
//        System.out.println("hello world");
//        System.out.println(new Date(265626565));
//        System.out.println(new GregorianCalendar(2018 ,12,05).getTime());
        GregorianCalendar d = new GregorianCalendar();
        int today = d.get(Calendar.DAY_OF_MONTH);
        int month = d.get(Calendar.MONTH);
        System.out.println(today);
        System.out.println(month + 1);
    }
}
