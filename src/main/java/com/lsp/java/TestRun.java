package com.lsp.java;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Date;

/**
 * @Auther: Lisp
 * @Date: 2018/12/5 17:30
 * @Description:
 */
public class TestRun {

    public static void main(String[] args) throws Exception {
        int a = 0;
        for (int i = 0; i < 10; i++) {
            System.out.println("初始：" + a);
            numAdd(a);
        }

    }


    public static void numAdd(int b) {
        b = b++;
        System.out.println("变化后的值：" + b);
    }
}
