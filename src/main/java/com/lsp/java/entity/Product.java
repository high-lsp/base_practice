package com.lsp.java.entity;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/2 17:24
 */
@Data
public class Product {
    private Long id;
    private Integer num;
    private BigDecimal price;
    private String name;
    private String category;

    public Product(Long id, Integer num, BigDecimal price, String name, String category) {
        this.id = id;
        this.num = num;
        this.price = price;
        this.name = name;
        this.category = category;
    }
}
