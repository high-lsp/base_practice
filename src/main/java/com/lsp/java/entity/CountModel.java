package com.lsp.java.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/11/2 12:01
 */
@Data
@ToString
public class CountModel {
    /**
     * 不去重统计
     */
    private int count;

    /**
     * 不去重统计
     */
    private int disCount;
    /**
     * 统计日期
     */
    private String formatDate;
}
