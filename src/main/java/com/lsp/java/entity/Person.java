package com.lsp.java.entity;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * @Auther: Lisp
 * @Date: 2018/12/7 11:09
 * @Description:
 */
public class Person {

    public Person() {
    }

    public Person(String n) {
        this.name = n;
    }

    private String name;

    private int age;

    public String getName() {
        return name;
    }

//    public abstract String getDescripthion();

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) throws Exception {
        Person person = new Person("张三");
        Class<? extends Person> aClass = person.getClass();
        Field[] declaredFields = aClass.getDeclaredFields();
        for (Field field : declaredFields) {
            String key = field.getName();
            PropertyDescriptor propertyDescriptor = new PropertyDescriptor(key, aClass);
            Method readMethod = propertyDescriptor.getReadMethod();
            Object invoke = readMethod.invoke(person);
            System.out.println(key + ":" + invoke);
            Method writeMethod = propertyDescriptor.getWriteMethod();
        }
    }
}
