package com.lsp.java.entity;

import java.util.Date;
import java.util.GregorianCalendar;

/**
 * @Auther: Lisp
 * @Date: 2018/12/5 17:26
 * @Description:
 */
public class Employee extends Person {

    public Employee() {
    }

    public Employee(String n, String m) {
        super(n);
        this.major = m;
    }

    public Employee(String n, double s) {
        this.name = n;
        this.salary = s;
    }

    private String major;

    public Employee(String n, double s, int year, int month, int day) {
        name = n;
        salary = s;
        GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        hireDay = calendar.getTime();
    }

    public String getName() {
        return name;
    }

//    @Override
//    public String getDescripthion() {
//        return "this is employee";
//    }

    public double getSalary() {
        return salary;
    }

    public Date getHireDay() {
        return hireDay;
    }

    private String name;
    private double salary;
    private Date hireDay;

    public final String sayHello() {
        return "hello world";
    }

    @Override
    public boolean equals(Object otherObject) {

        if (this == otherObject) return true;

        if (otherObject == null) return false;

        if (getClass() != otherObject.getClass()) return false;

        Employee other = (Employee) otherObject;

        return name.equals(other.getName()) && salary == other.getSalary() && hireDay.equals(other.getHireDay());

    }

    @Override
    public String toString() {
        return getClass().getName() + "::Employee{" +
                "major='" + major + '\'' +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", hireDay=" + hireDay +
                '}';
    }
}
