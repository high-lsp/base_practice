package com.lsp.java.entity;

import lombok.Data;
import lombok.ToString;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/12/29 17:19
 */
@Data
@ToString
public class CarInfo {

    public CarInfo(long csId, long carId, String mobile) {
        this.csId = csId;
        this.carId = carId;
        this.mobile = mobile;
    }

    /**
     * 车系ID
     */
    private long csId;
    /**
     * 车款ID
     */
    private long carId;
    /**
     * 手机号
     */
    private String mobile;
}
