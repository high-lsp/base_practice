package com.lsp.java.entity;

/**
 * @Auther: Lisp
 * @Date: 2018/12/20 13:37
 * @Description:
 */
public enum Size {
    SMALL("S"), MEDIUM("M"), LARGE("L"), EXTRA_LARGE("XL");

    private Size(String accbrea) {
        this.accbrea = accbrea;
    }

    private String accbrea;

    public String getAccbrea() {
        return accbrea;
    }
}
