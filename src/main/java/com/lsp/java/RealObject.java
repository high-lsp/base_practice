package com.lsp.java;

/**
 * @Auther: Lisp
 * @Date: 2019/1/7 14:30
 * @Description:
 */
public class RealObject implements Interface {


    @Override
    public void doSomething() {
        System.out.println("doSomething.");
    }

    @Override
    public void somethingElse(String arg) {
        System.out.println("somethingElse " + arg);
    }
}
