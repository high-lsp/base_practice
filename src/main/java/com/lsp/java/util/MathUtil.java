package com.lsp.java.util;

/**
 * 数学相关计算工具类
 *
 * @Author lisp
 * @Date 2022/1/29 下午4:35
 * @Version 1.0
 */
public class MathUtil {

    private MathUtil() {
        throw new IllegalStateException("MathUtil class");
    }

    /**
     * 传入一个数列x计算平均值
     *
     * @param x
     * @return 平均值
     */
    public static double average(double[] x) {
        //数列元素个数
        int n = x.length;
        double sum = 0;
        //求和
        for (double i : x) {
            sum += i;
        }
        return sum / n;
    }

    /**
     * 传入一个整数数列x计算平均值
     *
     * @param x
     * @return 平均值
     */
    public static double averageInt(int[] x) {
        //数列元素个数
        int n = x.length;
        double sum = 0;
        //求和
        for (double i : x) {
            sum += i;
        }
        return sum / n;
    }

    /**
     * 传入一个数列x计算方差
     * 方差s^2=[（x1-x）^2+（x2-x）^2+......（xn-x）^2]/（n）（x为平均数）
     *
     * @param x 要计算的数列
     * @return 方差
     */
    public static double variance(double[] x) {
        //数列元素个数
        int n = x.length;
        //求平均值
        double avg = average(x);
        double var = 0;
        for (double i : x) {
            //（x1-x）^2+（x2-x）^2+......（xn-x）^2
            var += (i - avg) * (i - avg);
        }
        return var / n;
    }

    /**
     * 传入一个数列x计算标准差
     * 标准差σ=sqrt(s^2)，即标准差=方差的平方根
     *
     * @param x 要计算的数列
     * @return 标准差
     */
    public static double standardDiviation(double[] x) {
        return Math.sqrt(variance(x));
    }


    /**
     * 误差范围
     * σ/sqrt(n)
     *
     * @param standardDiviationNum 标准差
     * @param countNum             统计数据个数
     * @return double
     **/
    public static double toleranceScope(double standardDiviationNum, int countNum) {
        return standardDiviationNum / Math.sqrt(countNum);
    }
}
