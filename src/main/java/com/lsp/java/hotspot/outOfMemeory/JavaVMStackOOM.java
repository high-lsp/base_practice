package com.lsp.java.hotspot.outOfMemeory;

/**
 * @author lisp
 * @ClassName JavaVMStackOOM
 * @Date 2019/6/10 10:31
 * VM args : -Xss2M
 */
public class JavaVMStackOOM {
    public void dontStop() {
        while (true) {

        }
    }

    public void satckLeakByThread() {
        while (true) {
            new Thread(() -> dontStop()).start();
        }
    }

    public static void main(String[] args) {
        JavaVMStackOOM oom = new JavaVMStackOOM();
        oom.satckLeakByThread();
    }
}
