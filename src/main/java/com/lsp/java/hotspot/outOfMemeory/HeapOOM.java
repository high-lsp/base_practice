package com.lsp.java.hotspot.outOfMemeory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lisp
 * @ClassName HeapOOM
 * @Date 2019/6/6 15:58
 * VM args :  -Xms20m -Xmx20m -XX:+HeapDumpOnOutOfMemoryError
 */
public class HeapOOM {

    static class OOMObject {
    }

    public static void main(String[] args) {
        List<OOMObject> list = new ArrayList<OOMObject>();
        while (true) {
            list.add(new OOMObject());
        }
    }

}
