package com.lsp.java.hotspot.outOfMemeory;

/**
 * @author lisp
 * @ClassName JavaVMStackOf
 * @Date 2019/6/6 17:19
 * VM args: -Xss128k
 */
public class JavaVMStackOF {
    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        stackLeak();
    }

    public static void main(String[] args) throws Throwable {
        JavaVMStackOF oom = new JavaVMStackOF();
        try {
            Thread.sleep(5000);
            oom.stackLeak();
        } catch (Throwable e) {
            System.out.println("stack length: " + oom.stackLength);
            throw e;
        }
    }
}
