package com.lsp.java;

/**
 * @Auther: Lisp
 * @Date: 2019/1/7 14:29
 * @Description:
 */
public interface Interface {
    void doSomething();

    void somethingElse(String arg);
}
