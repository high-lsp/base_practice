package com.lsp.java.jvm;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/4/19 17:24
 */
public class ReferenceCountingGc {

    Object instance = null;

    public static void main(String[] args) {
        ReferenceCountingGc objA = new ReferenceCountingGc();
        ReferenceCountingGc objB = new ReferenceCountingGc();
        objA.instance = objB;
        objB.instance = objA;
        objA = null;
        objB = null;
    }
}
