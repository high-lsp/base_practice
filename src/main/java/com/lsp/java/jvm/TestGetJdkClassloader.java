//package com.lsp.java.jvm;
//
//
//import javax.swing.*;
//import java.net.URL;
//
///**
// * @description:
// * @author: Lisp
// * @time: 2020/8/20 12:14
// */
//public class TestGetJdkClassloader {
//    public static void main(String[] args) {
//        System.out.println(String.class.getClassLoader());
//        System.out.println(com.sun.crypto.provider.DESKeyFactory.class.getClassLoader().getClass().getName());
//        System.out.println(TestGetJdkClassloader.class.getClassLoader().getClass().getName());
//
//        System.out.println();
//        ClassLoader appClassLoader = ClassLoader.getSystemClassLoader();
//        ClassLoader extClassloader = appClassLoader.getParent();
//        ClassLoader bootstrapLoader = extClassloader.getParent();
//        System.out.println("the bootstrapLoader : " + bootstrapLoader);
//        System.out.println("the extClassloader : " + extClassloader);
//        System.out.println("the appClassLoader : " + appClassLoader);
//
//        System.out.println();
//        System.out.println("bootstrapLoader加载以下文件：");
//        URL[] urls = Launcher.getBootstrapClassPath().getURLs();
//        for (int i = 0; i < urls.length; i++) {
//            System.out.println(urls[i]);
//        }
//
//        System.out.println();
//        System.out.println("extClassloader加载以下文件：");
//        System.out.println(System.getProperty("java.ext.dirs"));
//
//        System.out.println();
//        System.out.println("appClassLoader加载以下文件：");
//        System.out.println(System.getProperty("java.class.path"));
//    }
//}
