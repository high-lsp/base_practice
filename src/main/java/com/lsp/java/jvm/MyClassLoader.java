//package com.lsp.java.jvm;
//
//import java.io.FileInputStream;
//
///**
// * @Description
// * @Author lishengpeng
// * @Date 2022/4/15 11:16
// */
//public class MyClassLoader extends ClassLoader {
//
//    private String classPath;
//
//    public MyClassLoader(String classPath) {
//        this.classPath = classPath;
//    }
//
//    private byte[] loadByte(String name) throws Exception {
//        name = name.replaceAll("\\.", "/");
//        FileInputStream fis = new FileInputStream(classPath + "/" + name
//                + ".class");
//        int len = fis.available();
//        byte[] data = new byte[len];
//        fis.read(data);
//        fis.close();
//        return data;
//
//    }
//
//    @Override
//    protected Class<?> findClass(String name) throws ClassNotFoundException {
//        try {
//            byte[] data = loadByte(name);
//            return defineClass(name, data, 0, data.length);
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw new ClassNotFoundException();
//        }
//    }
//
//    /**
//     * 重写类加载方法，实现自己的加载逻辑，不委派给双亲加载
//     *
//     * @param name
//     * @param resolve
//     * @return
//     * @throws ClassNotFoundException
//     */
//    @Override
//    protected Class<?> loadClass(String name, boolean resolve)
//            throws ClassNotFoundException {
//        synchronized (getClassLoadingLock(name)) {
//            // First, check if the class has already been loaded
//            Class<?> c = findLoadedClass(name);
//
//            if (c == null) {
//                // If still not found, then invoke findClass in order
//                // to find the class.
//                long t1 = System.nanoTime();
//
//                //非自定义的类还是走双亲委派加载
//                if (!name.startsWith("com.lsp")) {
//                    c = this.getParent().loadClass(name);
//                } else {
//                    c = findClass(name);
//                }
//
//                // this is the defining class loader; record the stats
//                sun.misc.PerfCounter.getFindClassTime().addElapsedTimeFrom(t1);
//                sun.misc.PerfCounter.getFindClasses().increment();
//            }
//            if (resolve) {
//                resolveClass(c);
//            }
//            return c;
//        }
//    }
//}
