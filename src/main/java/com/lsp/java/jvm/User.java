package com.lsp.java.jvm;

import lombok.Data;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/8/20 12:47
 */
@Data
public class User {
    private Integer id;
    private String name;


    public User() {
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public void sout() {
        System.out.println("===myself class loader ====");
    }
}
