package com.lsp.java.jvm;

/**
 * @description: jvm 运行时时区
 * 线程私有的：
 * 栈（线程）：局部变量表、操作数栈（代码运行当中，数值的操作）、动态链接、
 * 方法出口（内部方法执行完之后，应该返回到调用方法的具体位置），
 * -Xss 设置单个线程栈内存的大小，值越大，能开的线程越少
 * 本地方法栈：
 * 程序计数器：
 * 共有的：
 * 堆：
 * 方法区（元空间，用的是直接内存）：常量、静态变量、类元信息（类的组成部分,*.class）
 * @author: Lisp
 * @time: 2020/8/20 20:09
 */
public class Math {

    public static int initData = 666;

    public int compute() { //一个方法对应一个栈帧内存区域
        int a = 1;
        int b = 2;
        int c = (a + b) * 10;
        return c;
    }

    public static void main(String[] args) throws Exception {

        try {
            Math math = new Math();
            int compute = math.compute();
            System.out.println(compute);
            throw new Exception();
        } catch (Exception e) {//验证 try catch finally，如果catch有返回值finally是否执行（执行）。
            e.printStackTrace();
            System.out.println("catch ing.......");
            return;
        } finally {
            System.out.println("finally is ope......");
        }
    }
}
