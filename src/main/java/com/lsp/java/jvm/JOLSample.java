package com.lsp.java.jvm;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/4/19 15:41
 */
public class JOLSample {


    /******************* 逃逸分析 *********************/
    //很显然test1方法中的user对象被返回了，这个对象的作用域范围不确定，
    // test2方法中的user对象我们可以确定当方法结束这个对象就可以认为是无效对象了，
    // 对于这样的对象我们其实可以将其分配在栈内存里，让其在方法结束时跟随栈内存一起被回收掉。
    public User test1() {
        User user = new User();
        user.setName("zs");
        user.setId(1);
        return user;
    }

    public void test2() {
        User user = new User();
        user.setName("zs");
        user.setId(1);
        //todo
    }

    /******************* 逃逸分析 end  *********************/


    public static void main(String[] args) {
        ClassLayout layout = ClassLayout.parseInstance(new Object());
        System.out.println(layout.toPrintable());

        System.out.println();
        ClassLayout layout1 = ClassLayout.parseInstance(new int[]{});
        System.out.println(layout1.toPrintable());

        System.out.println();
        ClassLayout layout2 = ClassLayout.parseInstance(new A());
        System.out.println(layout2.toPrintable());
    }


    // -XX:+UseCompressedOops           默认开启的压缩所有指针
    // -XX:+UseCompressedClassPointers  默认开启的压缩对象头里的类型指针Klass Pointer
    // Oops : Ordinary Object Pointers
    public static class A {
        //8B mark word
        //4B Klass Pointer   如果关闭压缩-XX:-UseCompressedClassPointers或-XX:-UseCompressedOops，则占用8B
        int id;        //4B
        String name;   //4B  如果关闭压缩-XX:-UseCompressedOops，则占用8B
        byte b;        //1B
        Object o;      //4B  如果关闭压缩-XX:-UseCompressedOops，则占用8B
    }
}
