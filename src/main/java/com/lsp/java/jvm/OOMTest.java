package com.lsp.java.jvm;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/4/19 17:29
 */
public class OOMTest {

    private static List<Object> list = new ArrayList<>();

    public static void main(String[] args) {
        int i = 0, j = 0;
        while (true) {
            list.add(new User(i++, UUID.randomUUID().toString()));
            new User(j--, UUID.randomUUID().toString());
        }
    }

    @Override
    protected void finalize() throws Throwable {
        OOMTest.list.add(this);
        System.out.println("关闭资源，userid=" + 1 + "即将被回收");
    }
}
