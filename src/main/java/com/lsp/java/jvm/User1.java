package com.lsp.java.jvm;

/**
 * @description:
 * @author: Lisp
 * @time: 2020/8/20 12:47
 */
public class User1 {
    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void sout() {
        System.out.println("===myself class loader ====");
    }
}
