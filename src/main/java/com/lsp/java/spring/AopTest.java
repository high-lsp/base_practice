package com.lsp.java.spring;

import com.lsp.java.spring.bean.AOPBean;
import com.lsp.java.spring.config.Myconfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/9 2:42
 */
public class AopTest {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(Myconfig.class);

        AOPBean bean = context.getBean(AOPBean.class);
        String s = bean.testAOP();
        System.out.println(s);

//        ProxyFactory proxyFactory = new ProxyFactory();
//
//        proxyFactory.setTarget(new UserService());
//        proxyFactory.addAdvice(new MethodInterceptor(){
//
//            @Override
//            public Object invoke(MethodInvocation invocation) throws Throwable {
//                System.out.println("before...");
//                Object result = invocation.proceed();
//                System.out.println("after...");
//                return result;
//            }
//        });
//        UserInterface proxy =(UserInterface) proxyFactory.getProxy();
//        proxy.test();
    }
}
