package com.lsp.java.spring.bean;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/9 3:22
 */
@Configuration
@Aspect
public class DefineAOP {

    @Pointcut("@annotation(com.lsp.java.spring.anno.CacheAOP)")
    public void pointcut() {
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("this is around method aop start ");
        Object proceed = joinPoint.proceed();
        System.out.println("self result " + proceed);
        System.out.println("this is around method aop end ");
        return proceed;
    }

    @Before("pointcut()")
    public void before() {
        System.out.println("=========before........");
    }

    @AfterReturning(pointcut = "pointcut()", returning = "result")
    public void afterReturning(Object result) {
        System.out.println("after sorce " + result);
    }
}
