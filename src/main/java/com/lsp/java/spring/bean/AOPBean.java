package com.lsp.java.spring.bean;

import com.lsp.java.spring.anno.CacheAOP;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/9 3:38
 */
@Component
public class AOPBean {

    @CacheAOP
    public String testAOP() {
        System.out.println("runnint method....");
        return "aopBean test";
    }

    public static void main(String[] args) {
        int num = 10;
        do {
            System.out.println("111");
            --num;
            System.out.println(num);
        } while (num == 10 || num > 0);

    }

}
