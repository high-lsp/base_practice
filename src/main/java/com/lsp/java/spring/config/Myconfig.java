package com.lsp.java.spring.config;

import com.lsp.java.spring.bean.AOPBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/9 2:44
 */
@EnableAspectJAutoProxy
@Configuration
@ComponentScan(basePackages = {"com.lsp.java.spring"})
public class Myconfig {

}
