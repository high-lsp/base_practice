package com.lsp.java.spring.file;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/10 9:53
 */
public interface UserInterface {
    void test();
}
