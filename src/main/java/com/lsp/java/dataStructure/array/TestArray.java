package com.lsp.java.dataStructure.array;

import java.util.Arrays;

public class TestArray {
    public static void main(String[] args) {
        ArrayUtils arrayUtils = new ArrayUtils();
        arrayUtils.add(1);
        arrayUtils.add(2);
        arrayUtils.add(3);
        arrayUtils.add(4);
        arrayUtils.add(5);
        arrayUtils.add(6);
        arrayUtils.add(7);
        arrayUtils.add(8);
        arrayUtils.show();
        int search = arrayUtils.binarySearch(4);
        int search2 = arrayUtils.binarySearch(0);
        int search3 = arrayUtils.binarySearch(10);
        System.out.println(search);
        System.out.println(search2);
        System.out.println(search3);


    }

    /***
     * @Description: 为数组新加一个元素
     * @Param:
     * @return: void
     * @Author: Lsp
     * @Date: 2019/12/20
     */
    public static void insert() {
        //数组的长度不可变
        int[] arrNum = new int[]{1, 2, 3, 4, 5};
        //重新创建一个数组，长度为 arrNum+1
        int[] newArr = new int[arrNum.length + 1];
        System.out.println(Arrays.toString(arrNum));
        int args = 3;
        for (int i = 0; i < arrNum.length; i++) {
            //把老数组复制到新的数组内
            newArr[i] = arrNum[i];
        }
        newArr[arrNum.length] = args;
        System.out.println(Arrays.toString(newArr));
    }

    public static void delete() {
        //数组的长度不可变
        int[] arrNum = new int[]{1, 2, 3, 4, 5};
        //重新创建一个数组，长度为 arrNum+1
        int[] newArr = new int[arrNum.length - 1];
        System.out.println(Arrays.toString(arrNum));
        int dele = 3;
        for (int i = 0; i < newArr.length; i++) {
            //把老数组复制到新的数组内
            if (i < dele) {
                newArr[i] = arrNum[i];
            } else {
                newArr[i] = arrNum[i + 1];
            }
        }

        System.out.println(Arrays.toString(newArr));
    }
}
