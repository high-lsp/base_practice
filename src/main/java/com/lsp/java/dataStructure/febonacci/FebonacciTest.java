package com.lsp.java.dataStructure.febonacci;

public class FebonacciTest {
    public static void main(String[] args) {
        //斐波那契数列：1 1 2 3 5 8 13，从第三项开始，每项的值等于前两项的和
        int febonacci = febonacci(6);
        System.out.println(febonacci);
    }

    public static int febonacci(int i) {
        if (i == 1 || i == 2) {
            return 1;
        } else {
            return febonacci(i - 1) + febonacci(i - 2);
        }
    }
}
