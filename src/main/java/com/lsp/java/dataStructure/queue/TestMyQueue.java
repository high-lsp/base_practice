package com.lsp.java.dataStructure.queue;

import com.lsp.java.dataStructure.stack.MyStack;

public class TestMyQueue {
    public static void main(String[] args) {
        MyQueue myQueue = new MyQueue();

        myQueue.add(1);
        myQueue.add(2);
        myQueue.add(3);
        myQueue.add(4);

        myQueue.show();

        myQueue.cosume();
        myQueue.cosume();
        myQueue.cosume();
        myQueue.show();
    }
}
