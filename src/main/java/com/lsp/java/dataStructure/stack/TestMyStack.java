package com.lsp.java.dataStructure.stack;

public class TestMyStack {
    public static void main(String[] args) {
        MyStack myStack = new MyStack();

        myStack.push(1);
        myStack.push(2);
        myStack.push(3);
        myStack.push(4);

        myStack.show();

        myStack.pop();
        myStack.pop();
        myStack.pop();
        myStack.show();
    }
}
