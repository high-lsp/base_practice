package com.lsp.java.dataStructure.stack;

import java.util.Arrays;

public class MyStack {

    private int[] elements;

    public MyStack() {
        this.elements = new int[0];
    }

    //显示所有元素
    public void show() {
        System.out.println(Arrays.toString(elements));
    }


    public void push(int element) {
        int[] newArr = new int[elements.length + 1];
        for (int i = 0; i < elements.length; i++) {
            //把老数组复制到新的数组内
            newArr[i] = elements[i];
        }
        newArr[elements.length] = element;
        elements = newArr;
    }

    public int pop() {
        int[] newArr = new int[elements.length - 1];
        int result = elements[elements.length - 1];
        for (int i = 0; i < elements.length - 1; i++) {
            newArr[i] = elements[i];
        }
        elements = newArr;
        return result;
    }
}
