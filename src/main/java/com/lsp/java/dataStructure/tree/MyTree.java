package com.lsp.java.dataStructure.tree;


import lombok.Data;

@Data
public class MyTree {

    private Node root;

    public boolean insert(long data, String sData) {
        Node node = new Node(data, sData);
        if (null == root) {
            root = node;
            return true;
        }
        Node current = root;
        while (true) {
            Node parent = current;
            current = current.getData() > data ? current.getLeft() : current.getRight();
            if (null == current) {
                if (parent.getData() > data) {
                    parent.setLeft(node);
                    return true;
                }
                parent.setRight(node);
                return true;
            }
        }
    }

    /***
     * @Description: 查找结点
     * @Param:
     * @param data
     * @return:
     * @Author: Lsp
     * @Date: 2019/12/19
     */
    public Node find(long data) {
        Node current = root;
        while (current.getData() != data) {
            current = current.getData() > data ? current.getLeft() : current.getRight();
            if (null == current) {
                return null;
            }
        }
        return current;
    }


    public static void main(String[] args) {
        MyTree myTree = new MyTree();
        myTree.insert(10, "zhangfei");
        myTree.insert(20, "guanyu");
        myTree.insert(15, "liubei");
        myTree.insert(3, "lubu");
        myTree.insert(4, "caocao");
        myTree.insert(90, "xiaoqiao");
        myTree.insert(2, "daqiao");

        System.out.println(myTree.toString());
        System.out.println("-------");
        System.out.println(myTree.find(2).getData() + myTree.find(2).getSData());
    }
}
