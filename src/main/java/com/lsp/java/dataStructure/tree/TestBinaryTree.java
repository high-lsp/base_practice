package com.lsp.java.dataStructure.tree;

public class TestBinaryTree {

    public static void main(String[] args) {
        //创建一棵树
        BinaryTree binaryTree = new BinaryTree();
        //创建一个根结点
        TreeNode root = new TreeNode(1);

        binaryTree.setRoot(root);

        TreeNode rootL = new TreeNode(2);
        TreeNode rootR = new TreeNode(3);

        root.setLeftNode(rootL);
        root.setRightNode(rootR);

        rootL.setLeftNode(new TreeNode(4));
        rootL.setRightNode(new TreeNode(5));

        rootR.setLeftNode(new TreeNode(6));
        rootR.setRightNode(new TreeNode(7));

        //前序遍历
        binaryTree.frontShow();
        //中序遍历
        System.out.println("-------------------------");
        binaryTree.midShow();
        //后序遍历
        System.out.println("-------------------------");
        binaryTree.afterShow();

        System.out.println("++++++++=========");
        TreeNode treeNode = binaryTree.frontSearch(5);
        System.out.println(treeNode.value);
        binaryTree.delete(1);
        binaryTree.delete(2);
        binaryTree.delete(5);
        System.out.println("================");
        binaryTree.frontShow();

    }
}
