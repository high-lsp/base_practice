package com.lsp.java.dataStructure.tree;

import lombok.Data;

@Data
public class Node {

    //数据项
    private long data;

    private String sData;

    /**
     * 左子结点
     */
    private Node left;

    /***
     * 右子结点
     */
    private Node right;


    public Node(long data, String sData) {
        this.data = data;
        this.sData = sData;
    }

}
