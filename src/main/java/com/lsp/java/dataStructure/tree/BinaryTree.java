package com.lsp.java.dataStructure.tree;

public class BinaryTree {

    //根节点
    TreeNode root;

    //设置根节点
    public void setRoot(TreeNode root) {
        this.root = root;
    }

    //获取根节点
    public TreeNode getRoot() {
        return root;
    }

    public void frontShow() {
        if (root != null) {
            root.frontShow();

        }
    }

    public void midShow() {
        if (root != null) {
            root.midShow();

        }
    }

    public void afterShow() {
        if (root != null) {
            root.afterShow();
        }
    }

    public TreeNode frontSearch(int i) {
        return root.frontSearch(i);
    }

    public void delete(int i) {
        if (root != null && root.value == i) {
            root = null;
        } else {
            if (root != null) {
                root.delete(i);
            }
        }
    }
}
