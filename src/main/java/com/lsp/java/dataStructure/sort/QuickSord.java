package com.lsp.java.dataStructure.sort;

import java.util.Arrays;

public class QuickSord {
    public static void main(String[] args) {
        int[] arr = new int[]{5, 9, 7, 8, 3, 2, 4, 15, 6};
        System.out.println(Arrays.toString(arr));
        quickSord(arr, 0, arr.length - 1);
        System.out.println(Arrays.toString(arr));
    }

    /***
     * @Description: 冒泡排序
     * @Param:
     * @param arr
     * @return: void
     * @Author: Lsp
     * @Date: 2019/12/26
     */
    public static void quickSord(int[] arr, int start, int end) {
        if (start < end) {
            //把第 0 个数字作为标准数
            int stard = arr[start];
            //记录需要排序的下标
            int low = start;
            int high = end;
            //循环找比标准数大的数，和比标准数小的数
            while (low < high) {
                //右边的数字比标准数大
                while (low < high && stard < arr[high]) {
                    high--;
                }
                //使用右边的数字替换左边的数
                arr[low] = arr[high];
                //如果左边的数字比标准数小
                while (low < high && arr[low] <= stard) {
                    low++;
                }
                arr[high] = arr[low];
            }
            //把标准数赋给低（高）位置所在的元素
            arr[low] = stard;
            //处理所有小的数字
            quickSord(arr, start, low);
            //处理所有大的数字
            quickSord(arr, low + 1, end);
        }
    }

}
