package com.lsp.java.dataStructure.sort;

import java.util.Arrays;

public class BubbleSord {
    public static void main(String[] args) {
        int[] arr = new int[]{5, 9, 7, 8, 3, 2, 4, 15, 6};
        System.out.println(Arrays.toString(arr));
        bubbleSord(arr);
        System.out.println(Arrays.toString(arr));
    }

    /***
     * @Description: 冒泡排序
     * @Param:
     * @param arr
     * @return: void
     * @Author: Lsp
     * @Date: 2019/12/26
     */
    public static void bubbleSord(int[] arr) {
        //控制共比较多少轮
        for (int i = 0; i < arr.length - 1; i++) {
            //比较次数
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }
}
