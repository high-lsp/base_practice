package com.lsp.java.dataStructure.linked;

public class TestNode {

    public static void main(String[] args) {
        Node n1 = new Node(1);
        Node n2 = new Node(2);
        Node n3 = new Node(3);
        Node n4 = new Node(4);
        Node n5 = new Node(5);

        n1.append(n2).append(n3).append(n4);

        n1.show();
        n1.next().after(n5);
        n1.show();
//        System.out.println(n1.toString());
//        System.out.println(n1.next().toString());
//        n1.removeNext();
//        System.out.println(n1.next().toString());

    }
}
