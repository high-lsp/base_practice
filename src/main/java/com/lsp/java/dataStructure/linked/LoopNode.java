package com.lsp.java.dataStructure.linked;

import lombok.Data;

/***
 * @Description: 循环链表
 * @Param:
 * @param
 * @return:
 * @Author: Lsp
 * @Date: 2019/12/25
 */
@Data
public class LoopNode {

    private int data;

    private LoopNode next = this;

    public LoopNode(int data) {
        this.data = data;
    }

    //删除下一个结点
    public void removeNext() {
        //取出下下一个结点
        LoopNode newNext = next.next;
        //把下下一个结点作为当前节点的下一个结点
        this.next = newNext;
    }


    //插入一个结点作为当前结点的下一个结点
    public void after(LoopNode node) {
        //取出下一个结点，作为下下一个结点
        LoopNode nextNext = next;
        //把新结点作为当前结点的下一个结点
        this.next = node;
        //把下下一个结点设置为新结点的下一个结点
        node.next = nextNext;
    }
}
