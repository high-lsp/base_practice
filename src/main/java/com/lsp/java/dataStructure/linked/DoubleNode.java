package com.lsp.java.dataStructure.linked;

/***
 * @Description: 双链表：每一个结点，即可以找到它的上一个结点，也可以找到它的下一个结点
 * @Param:
 * @return:
 * @Author: Lsp
 * @Date: 2019/12/25
 */
public class DoubleNode {

    //上一个结点
    DoubleNode pre = this;
    //下一个结点
    DoubleNode next = this;
    //结点数据
    int data;

    public DoubleNode(int data) {
        this.data = data;
    }

    //增加结点
    public void after(DoubleNode node) {
        //原来的下一个结点
        DoubleNode nextNext = next;
        //把新结点当为当前结点的下一个结点
        this.next = node;
        //把当前结点作为新结点的前一个结点
        node.pre = this;
        //让原来的下一个结点作为当前结点的先一个结点
        node.next = nextNext;
        //让原来的结点的下一个结点的上一个结点为新结点
        nextNext.pre = node;
    }

    //下一个结点
    public DoubleNode next() {
        return this.next;
    }

    //上一个结点
    public DoubleNode pre() {
        return this.pre;
    }

    //上一个结点
    public int getData() {
        return this.data;
    }
}
