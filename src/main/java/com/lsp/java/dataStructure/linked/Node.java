package com.lsp.java.dataStructure.linked;

import lombok.Data;

/***
 * @Description: 单链表
 * @Param:
 * @param
 * @return:
 * @Author: Lsp
 * @Date: 2019/12/25
 */
@Data
public class Node {

    private int data;

    private Node next;

    public Node(int data) {
        this.data = data;
    }

    //追加结点
    public Node append(Node node) {
//        Node append = this.next;
//        if (null == append) {
//            this.next = node;
//        }else {
//            append.next = node;
//        }
        //当前节点
        Node currentNode = this;
        //循环向后找
        while (true) {
            Node nextNode = currentNode.next;
            if (null == nextNode) {
                break;
            }
            //如果没有下一个节点，当前节点是最后一个节点
            currentNode = nextNode;
        }
        //把需要追回的结点追加为找到的当前的结点的下一个结点
        currentNode.next = node;
        return this;
    }

    public Node next() {
        return this.next;
    }

    public boolean isLast() {
        return null == this.next;
    }

    //删除下一个结点
    public void removeNext() {
        //取出下下一个结点
        Node newNext = next.next;
        //把下下一个结点作为当前节点的下一个结点
        this.next = newNext;
    }

    //显示所有结点信息
    public void show() {
        Node currentNode = this;
        while (true) {
            System.out.print(currentNode.data + "");
            //取出下一个结点
            currentNode = currentNode.next;
            //如果下一个结点为空，则是最后一个结点
            if (null == currentNode) {
                break;
            }

        }
    }

    //插入一个结点作为当前结点的下一个结点
    public void after(Node node) {
        //取出下一个结点，作为下下一个结点
        Node nextNext = next;
        //把新结点作为当前结点的下一个结点
        this.next = node;
        //把下下一个结点设置为新结点的下一个结点
        node.next = nextNext;
    }
}
