package com.lsp.java.dataStructure.linked;

/**
 * @description: 双向链表
 * @author: Lisp
 * @time: 2020/9/3 17:54
 */
public class MyDoubleLinkedList {
    private DNode head; //头
    private DNode tail; //尾

    MyDoubleLinkedList() {
        head = null;
        tail = null;
    }

    //插入头部
    public void insertHead(int data) {
        DNode newNode = new DNode(data);
        if (head == null) {
            tail = newNode;
        } else {
            head.pre = newNode;
            newNode.next = head;
        }
        head = newNode;
    }

    //删除
    public void deleteHead() {
        DNode cur = head.next;
        cur.pre = null;
        head = cur;
    }
}

class DNode {
    int value;//值
    DNode next; //下一个指针
    DNode pre;  //前一个指针

    public DNode(int value) {
        this.value = value;
        this.next = null;//最开始的时候下一个结点为 null
        this.pre = null;
    }

}

