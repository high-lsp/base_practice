package com.lsp.java.jdk8;

public interface FuncInterface<T> {

    boolean test(T param);
}
