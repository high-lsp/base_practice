package com.lsp.java.jdk8;

/**
 * @author lisp
 * @ClassName MyStudyFunMethod
 * @Date 2019/5/31 15:37
 */
public class MyStudyFunMethod {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("hello world");
            }
        }).run();


        new Thread(() -> System.out.println("this is hello world")).run();

        funTest((Integer x) -> true);
    }


    public static void funTest(FuncInterface<Integer> funcInterface) {
        int a = 1;
        funcInterface.test(a);
    }
}
