package com.lsp.java.jdk8;

import com.alibaba.fastjson.JSON;
import com.lsp.java.entity.Product;
import org.apache.commons.collections4.ListUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Auther: Lisp
 * @Date: 2019/1/11 13:22
 * @Description:
 */
public class StreamApi {

    public static void main(String[] args) {
        Product prod1 = new Product(1L, 1, new BigDecimal("15.5"), "面包", "零食");
        Product prod2 = new Product(2L, 2, new BigDecimal("20"), "饼干", "零食");
        Product prod3 = new Product(3L, 3, new BigDecimal("30"), "月饼", "零食");
        Product prod4 = new Product(4L, 3, new BigDecimal("10"), "青岛啤酒", "啤酒");
        Product prod5 = new Product(5L, 10, new BigDecimal("15"), "百威啤酒", "啤酒");
        List<Product> prodList = new ArrayList<>();
        prodList.add(prod1);
        prodList.add(prod2);
        prodList.add(prod3);
        prodList.add(prod4);
        prodList.add(prod5);
        //按照类目分组
        Map<String, List<Product>> prodMap = prodList.stream().collect(Collectors.groupingBy(Product::getCategory));
        System.out.println(JSON.toJSONString(prodMap));
        //按照几个属性拼接分组
        Map<String, List<Product>> prodMap2 =
                prodList.stream().collect(Collectors.groupingBy(item -> item.getCategory() + "_" + item.getName()));
        System.out.println(JSON.toJSONString(prodMap2));
        //根据不同条件分组
        Map<String, List<Product>> prodMap3 = prodList.stream().collect(Collectors.groupingBy(item -> {
            if (item.getNum() < 3) {
                return "3";
            } else {
                return "other";
            }
        }));
        System.out.println(JSON.toJSONString(prodMap3));
        //多级分组
        Map<String, Map<String, List<Product>>> prodMap4 =
                prodList.stream().collect(Collectors.groupingBy(Product::getCategory, Collectors.groupingBy(item -> {
                    if (item.getNum() < 3) {
                        return "3";
                    } else {
                        return "other";
                    }
                })));
        System.out.println(JSON.toJSONString(prodMap4));
        /***************************************按子组收集数据*******************/
        //求总数
        Map<String, Long> prodMap5 = prodList.stream().collect(Collectors.groupingBy(Product::getCategory,
                Collectors.counting()));
        System.out.println(JSON.toJSONString(prodMap5));
        //求和
        Map<String, Integer> prodMap6 = prodList.stream().collect(Collectors.groupingBy(Product::getCategory,
                Collectors.summingInt(Product::getNum)));
        System.out.println(JSON.toJSONString(prodMap6));
        //多字段求和
        List<Product> collect = prodList.stream().collect(Collectors.groupingBy(Product::getCategory, Collectors.reducing(
                (sum, n) -> {
                    Product product = new Product(n.getId(), sum.getNum() + n.getNum(), sum.getPrice().add(n.getPrice()),
                            n.getName(),
                            n.getCategory());
                    return product;
                }
        ))).entrySet().stream().map(c -> c.getValue().get())
                .collect(Collectors.toList());
        System.out.println(JSON.toJSONString(collect));
    }
}
