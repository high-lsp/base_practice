package com.lsp.java.jdk8;

import com.lsp.java.entity.CarInfo;
import com.lsp.java.entity.CountModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeSet;
import java.util.stream.Collectors;


/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/11/2 11:53
 */
public class ListPritace {


    public static void main(String[] args) {
        filter();
    }

    /**
     * 过滤
     **/
    public static void filter() {
        List<CarInfo> list = new ArrayList<>();
        CarInfo c1 = new CarInfo(1, 1, "18300602017");
        CarInfo c2 = new CarInfo(2, 2, "18300602017");
        CarInfo c3 = new CarInfo(1, 2, "18300602017");
        CarInfo c4 = new CarInfo(2, 1, "18300602017");
        CarInfo c5 = new CarInfo(3, 3, "18300602017");
        CarInfo c6 = new CarInfo(1, 1, "18300602017");
        CarInfo c7 = new CarInfo(1, 1, "18300602017");
        CarInfo c8 = new CarInfo(1, 1, "18300602017");
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        list.add(c6);
        list.add(c7);
        list.add(c8);
        List<CarInfo> collect = list.stream().filter(carInfo -> carInfo.getCsId() > 1).collect(Collectors.toList());
        System.out.println(collect);
    }

    /**
     * 过滤两个list
     **/
    public static void filterTwoList() {
        List<CountModel> list1 = new ArrayList<>();
        CountModel m1 = new CountModel();
        m1.setFormatDate("2022-11-01");
        m1.setCount(11);
        list1.add(m1);
        List<CountModel> list2 = new ArrayList<>();
        CountModel m2 = new CountModel();
        m2.setFormatDate("2022-11-01");
        m2.setCount(222);
        list2.add(m2);


        //过滤两个list中相同的属性值的数据
        List<CountModel> list3 = list1.stream().parallel().filter(a -> list2.stream()
                .anyMatch(b ->
                        Objects.equals(a.getFormatDate(), b.getFormatDate()

                        ))).collect(Collectors.toList());

        List<CountModel> list4 = list1.stream().parallel().filter(a -> list2.stream()
                .noneMatch(b -> Objects.equals(a.getFormatDate(), b.getFormatDate()))).collect(Collectors.toList());
        System.out.println(list3);
        System.out.println(list4);
        List<CountModel> collect = list1.stream().map(m -> {
            list2.stream().filter(s -> Objects.equals(m.getFormatDate(), s.getFormatDate())).forEach(s -> {
                m.setDisCount(s.getCount());
            });
            return m;
        }).collect(Collectors.toList());

        System.out.println(collect);
    }

    /**
     * 去重
     **/
    public static void distinct() {
        List<CarInfo> list = new ArrayList<>();
        CarInfo c1 = new CarInfo(1, 1, "18300602017");
        CarInfo c2 = new CarInfo(2, 2, "18300602017");
        CarInfo c3 = new CarInfo(1, 2, "18300602017");
        CarInfo c4 = new CarInfo(2, 1, "18300602017");
        CarInfo c5 = new CarInfo(3, 3, "18300602017");
        CarInfo c6 = new CarInfo(1, 1, "18300602017");
        CarInfo c7 = new CarInfo(1, 1, "18300602017");
        CarInfo c8 = new CarInfo(1, 1, "18300602017");
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        list.add(c6);
        list.add(c7);
        list.add(c8);
        System.out.println(list);
        //单属性去重
        ArrayList<CarInfo> carInfos = list.stream().collect(Collectors.collectingAndThen(
                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(CarInfo::getCsId))), ArrayList::new));
        System.out.println(carInfos);
        //多属性去重
        ArrayList<CarInfo> carInfos2 = list.stream().collect(Collectors.collectingAndThen(
                Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(CarInfo::getCsId)
                        .thenComparing(CarInfo::getCarId)
                        .thenComparing(CarInfo::getMobile))),
                ArrayList::new));
        System.out.println(carInfos2);
        Map<Long, List<CarInfo>> collect = list.stream().collect(Collectors.groupingBy(CarInfo::getCsId));
        System.out.println(collect);
    }
}
