package com.lsp.java.dataOpe;

import com.alibaba.fastjson.JSONObject;

import com.lsp.java.entity.SysRole;
import com.lsp.java.util.DBUtil;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import java.util.List;

public class MyDataOpe {

    private static final String querySql = "SELECT * FROM sys_role ";

    public static void main(String[] args) throws Exception {
        queryListPool();
    }

    //数据库直连
    public static String queryList() throws Exception {
        QueryRunner queryRunner = new QueryRunner();
        List<SysRole> query = queryRunner.query(DBUtil.getConnection(), querySql, new BeanListHandler<SysRole>(SysRole.class));
        System.out.println(JSONObject.toJSONString(query));
        return JSONObject.toJSONString(query);
    }

    public static String queryListPool() throws Exception {
        QueryRunner queryRunner = new QueryRunner();
        List<SysRole> query = queryRunner.query(DBUtil.getPoolConnection(), querySql, new BeanListHandler<SysRole>(SysRole.class));
        System.out.println(JSONObject.toJSONString(query));
        return JSONObject.toJSONString(query);
    }

}
