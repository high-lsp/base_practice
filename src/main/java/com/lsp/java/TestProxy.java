package com.lsp.java;

import java.lang.reflect.Proxy;

/**
 * @Auther: Lisp
 * @Date: 2019/1/7 14:57
 * @Description:
 */
public class TestProxy {
    public static void main(String[] args) {
        RealObject realObject = new RealObject();
        Interface proxy = (Interface) Proxy.newProxyInstance(Interface.class.getClassLoader(), new Class[]{Interface.class}, new DynamicProxyHandler(realObject));
        proxy.doSomething();
        proxy.somethingElse("hello world");
    }
}
