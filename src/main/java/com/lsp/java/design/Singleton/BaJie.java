package com.lsp.java.design.Singleton;

import javax.swing.*;

/**
 * @Auther: Lisp
 * @Date: 2019/1/9 14:27
 * @Description:
 */
public class BaJie extends JPanel {

    private static final BaJie instance = new BaJie();

    private BaJie() {
        JLabel ll = new JLabel(new ImageIcon("src/Bajie.jpg"), JLabel.CENTER);
        this.add(ll);
    }

    public static BaJie getInstance() {
        return instance;
    }
}
