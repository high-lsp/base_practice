package com.lsp.java.design.Singleton;

/**
 * @Auther: Lisp
 * @Date: 2019/1/9 13:36
 * @Description: 饿汉单例模式
 */
public class HungrySingleton {

    private static final HungrySingleton hungreSingleton = new HungrySingleton();

    private HungrySingleton() {
    }

    public HungrySingleton getInstance() {
        return hungreSingleton;
    }
}
