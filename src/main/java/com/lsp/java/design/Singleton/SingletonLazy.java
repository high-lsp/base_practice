package com.lsp.java.design.Singleton;

/**
 * @Auther: Lisp
 * @Date: 2019/1/8 18:55
 * @Description:
 */
public class SingletonLazy {

    public static void main(String[] args) {
        LazySingleton zt1 = LazySingleton.getInstance();
        zt1.getName();    //输出总统的名字
        LazySingleton zt2 = LazySingleton.getInstance();
        zt2.getName();    //输出总统的名字
        if (zt1 == zt2) {
            System.out.println("他们是同一人！");
        } else {
            System.out.println("他们不是同一人！");
        }
    }
}
