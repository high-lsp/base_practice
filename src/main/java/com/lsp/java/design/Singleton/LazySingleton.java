package com.lsp.java.design.Singleton;

/**
 * @Auther: Lisp
 * @Date: 2019/1/8 18:45
 * @Description: 单例模式，包含一个实例且只能自己创建这个实例的类
 * 访问类：使用单例的类
 */
public class LazySingleton {

    private static volatile LazySingleton lazySingleton = null;

    private LazySingleton() {
        System.out.println("产生一个总统!");
    }

    public static synchronized LazySingleton getInstance() {
        if (lazySingleton == null) {
            lazySingleton = new LazySingleton();
        } else {
            System.out.println("已经有一个总统，不能产生一个新总统！");
        }
        return lazySingleton;
    }

    public void getName() {
        System.out.println("我是美国总统：特朗普。");
    }

}

