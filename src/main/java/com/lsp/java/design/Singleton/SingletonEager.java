package com.lsp.java.design.Singleton;

import javax.swing.*;
import java.awt.*;

/**
 * @Auther: Lisp
 * @Date: 2019/1/9 14:31
 * @Description:
 */
public class SingletonEager {
    public static void main(String[] args) {
        JFrame jf = new JFrame("饿汉单例模式测试");
        jf.setLayout(new GridLayout(1, 2));
        Container contentPane = jf.getContentPane();
        BaJie obj1 = BaJie.getInstance();
        contentPane.add(obj1);
        BaJie obj2 = BaJie.getInstance();
        contentPane.add(obj2);
        if (obj1 == obj2) {
            System.out.println("是同一个人");
        } else {
            System.out.println("不是同一个人");
        }
        jf.pack();
        jf.setVisible(true);
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
