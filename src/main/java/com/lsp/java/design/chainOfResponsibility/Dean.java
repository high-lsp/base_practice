package com.lsp.java.design.chainOfResponsibility;

/**
 * @Auther: Lisp
 * @Date: 2019/1/11 14:52
 * @Description:具体处理者3：院长类
 */
public class Dean extends Leader {
    @Override
    public void handleResult(int LeaveDays) {
        if (LeaveDays <= 10) {
            System.out.println("院长批准您请假" + LeaveDays + "天。");
        } else {
            if (getNext() != null) {
                getNext().handleResult(LeaveDays);
            } else {
                System.out.println("请假天数太多，没有人批准该假条！");
            }
        }
    }
}
