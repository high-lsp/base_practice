package com.lsp.java.design.chainOfResponsibility;

/**
 * @Auther: Lisp
 * @Date: 2019/1/11 14:44
 * @Description:
 */
public abstract class Leader {

    private Leader next;


    public Leader getNext() {
        return next;
    }

    public void setNext(Leader next) {
        this.next = next;
    }

    //定义处理请求的方法
    public abstract void handleResult(int leaveDays);
}
