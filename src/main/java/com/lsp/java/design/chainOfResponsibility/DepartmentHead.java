package com.lsp.java.design.chainOfResponsibility;

/**
 * @Auther: Lisp
 * @Date: 2019/1/11 14:50
 * @Description: 具体处理者2：系主任类
 */
public class DepartmentHead extends Leader {

    @Override
    public void handleResult(int LeaveDays) {
        if (LeaveDays <= 7) {
            System.out.println("系主任批准您请假" + LeaveDays + "天。");
        } else {
            if (getNext() != null) {
                getNext().handleResult(LeaveDays);
            } else {
                System.out.println("请假天数太多，没有人批准该假条！");
            }
        }
    }
}
