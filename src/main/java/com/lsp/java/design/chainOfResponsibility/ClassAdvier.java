package com.lsp.java.design.chainOfResponsibility;

/**
 * @Auther: Lisp
 * @Date: 2019/1/11 14:47
 * @Description: 班主任类
 */
public class ClassAdvier extends Leader {

    @Override
    public void handleResult(int leaveDays) {
        if (leaveDays < 2) {
            System.out.println("班主任批准您请假" + leaveDays + "天。");
        } else {
            if (getNext() != null) {
                getNext().handleResult(leaveDays);
            } else {
                System.out.println("请假天数太多，没有人批准该假条！");
            }
        }
    }
}
