package main.java.com.lsp.java.design.observer.rate;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:35
 * @Description:
 */
public class RateClient {
    public static void main(String[] args) {
        Rate rate = new RMBrate();

        Company exportCompant = new ExportCompany();
        Company importCompany = new ImportCompany();

        rate.add(exportCompant);
        rate.add(importCompany);

        rate.change(1);
        rate.change(-10);

    }
}
