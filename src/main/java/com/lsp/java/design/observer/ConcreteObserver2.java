package com.lsp.java.design.observer;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 14:57
 * @Description:
 */
public class ConcreteObserver2 implements Observer {
    @Override
    public void response() {
        System.out.println("具体观察者2作出反应！");
    }
}
