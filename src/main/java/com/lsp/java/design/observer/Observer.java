package com.lsp.java.design.observer;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 14:56
 * @Description: 抽象观察者
 */
public interface Observer {
    void response(); //反应
}
