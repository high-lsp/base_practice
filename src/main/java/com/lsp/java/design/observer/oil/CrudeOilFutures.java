package com.lsp.java.design.observer.oil;

import java.util.Observer;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 16:29
 * @Description:
 */
public class CrudeOilFutures {
    public static void main(String[] args) {
        OilFutures oil = new OilFutures();
        Observer bull = new Bull(); //多方
        Observer bear = new Bear(); //空方

        oil.addObserver(bull);
        oil.addObserver(bear);

        oil.setPrice(10);
        oil.setPrice(-8);
    }
}
