package com.lsp.java.design.observer.BellRing;

import com.lsp.java.design.observer.BellRing.BellEventListener;
import com.lsp.java.design.observer.BellRing.RingEvent;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:59
 * @Description:
 */
public class StuEventListener implements BellEventListener {
    @Override
    public void heardBell(RingEvent e) {
        if (e.getSound()) {
            System.out.println("同学们，上课了...");
        } else {
            System.out.println("同学们，下课了...");
        }
    }
}
