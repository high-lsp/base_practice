package main.java.com.lsp.java.design.observer.rate;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:30
 * @Description:
 */

public interface Company {
    void response(int number);
}
