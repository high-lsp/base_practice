package com.lsp.java.design.observer.BellRing;

import java.util.EventListener;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:55
 * @Description:
 */
public interface BellEventListener extends EventListener {

    //事件处理方法，听到铃声
    public void heardBell(RingEvent e);
}
