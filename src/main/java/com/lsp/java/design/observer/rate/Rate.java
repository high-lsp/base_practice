package main.java.com.lsp.java.design.observer.rate;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:29
 * @Description: 抽象目标：汇率
 * 利用观察者模式设计一个程序，分析“人民币汇率”的升值或贬值对进口公司的进口产品成本或出口公司的出口产品收入以及公司的利润率的影响。
 * <p>
 * 分析：当“人民币汇率”升值时，进口公司的进口产品成本降低且利润率提升，出口公司的出口产品收入降低且利润率降低；
 * 当“人民币汇率”贬值时，进口公司的进口产品成本提升且利润率降低，出口公司的出口产品收入提升且利润率提升。
 */
public abstract class Rate {

    protected List<Company> companies = new ArrayList<>();

    public void add(Company company) {
        companies.add(company);
    }

    public void remove(Company company) {
        companies.remove(company);
    }

    public abstract void change(int number);
}
