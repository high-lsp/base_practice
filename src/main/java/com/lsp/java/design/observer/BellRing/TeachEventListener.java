package com.lsp.java.design.observer.BellRing;

import com.lsp.java.design.observer.BellRing.BellEventListener;
import com.lsp.java.design.observer.BellRing.RingEvent;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:59
 * @Description:
 */
public class TeachEventListener implements BellEventListener {
    @Override
    public void heardBell(RingEvent e) {
        if (e.getSound()) {
            System.out.println("老师上课了...");
        } else {
            System.out.println("老师下课了...");
        }
    }
}
