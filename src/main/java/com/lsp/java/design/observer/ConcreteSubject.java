package com.lsp.java.design.observer;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:04
 * @Description:
 */
public class ConcreteSubject extends Subject {

    @Override
    public void notifyObserver() {
        System.out.println("具体目标发生改变...");
        System.out.println("--------------");

        for (Object obs : observers) {
            ((Observer) obs).response();
        }
    }

}
