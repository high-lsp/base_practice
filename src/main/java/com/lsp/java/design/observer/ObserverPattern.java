package com.lsp.java.design.observer;

import com.lsp.java.design.observer.*;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 14:54
 * @Description:
 */
public class ObserverPattern {

    public static void main(String[] args) {
        Subject subject = new ConcreteSubject();

        Observer observer1 = new ConcreteObserver1();
        Observer observer2 = new ConcreteObserver2();
        subject.add(observer1);
        subject.add(observer2);

        subject.notifyObserver();
    }
}
