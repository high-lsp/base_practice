package main.java.com.lsp.java.design.observer.rate;

/**
 * @Auther: Lisp
 * @Date: 2019/1/17 15:32
 * @Description:
 */
public class RMBrate extends Rate {
    @Override
    public void change(int number) {
        for (Company obs : companies) {
            ((Company) obs).response(number);
        }
    }
}
