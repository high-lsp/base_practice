package com.lsp.java.tread.threadpool;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/27 12:02
 */
@Slf4j
public class ThreadPoolExecutorExceptionDemo {

    public static void main(String[] args) {
        //创建一个线程池，指定核心线程、最大线程数为5，任务队列的大小为10
        ThreadPoolExecutor executor = new MyThreadPoolExecutor(5, 5, 0, TimeUnit.SECONDS, new LinkedBlockingQueue<>(10));
        //提交任务到线程池
        for (int i = 0; i < 10; i++) {
            executor.execute(() -> {
                //            int res = 1 / 0;  //这行代码会抛出异常，除数不能为0
                System.out.println(Thread.currentThread().getId());
                log.info("333");
            });
        }
        log.info("执行成功");
    }
}
