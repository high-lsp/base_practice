package com.lsp.java.tread.threadpool;

import lombok.SneakyThrows;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/27 12:06
 */
public class MyThreadPoolExecutor extends ThreadPoolExecutor {
    public MyThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    //重写ThreadPoolExecutor的afterExecute方法，实现对异常信息的全局处理
    @SneakyThrows
    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        //Throwable不为null，则说明任务执行过程中抛出了异常
        if (t != null) {
            System.out.println("任务执行抛出异常了，需要记录下来任务的异常信息");
            throw t;
        }
    }
}
