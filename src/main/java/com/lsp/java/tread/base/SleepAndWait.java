package com.lsp.java.tread.base;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/3/1 15:15
 */
@Slf4j
public class SleepAndWait {

    private static int a = 1;

    public static void numAdd() throws InterruptedException {
        synchronized (SleepAndWait.class) {
            if (Thread.currentThread().getName().equals("1")) {
                log.info("{}开始休眠,a={}", Thread.currentThread().getName(), a);
                Thread.sleep(100000);
            }
            a++;
            log.info("a={},id:{}", a, Thread.currentThread().getName());
        }
    }

    public static void main(String[] args) {

        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                try {
                    numAdd();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, i + "").start();
        }

    }
}
