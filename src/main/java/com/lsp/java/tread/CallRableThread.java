package com.lsp.java.tread;

import org.springframework.util.StopWatch;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/17 15:44
 */
public class CallRableThread implements Callable {

    @Override
    public Object call() throws Exception {
        StopWatch stopWatch = new StopWatch("报价任务开始记时");
        stopWatch.start();
        try {
            Thread.sleep(2000);
        } catch (Exception ex) {
            System.out.println("1111");
        } finally {
            stopWatch.stop();
        }
        System.out.println(stopWatch.prettyPrint() + "tname:" + Thread.currentThread().getName());
        return String.valueOf(stopWatch.getTotalTimeSeconds());
    }


    public static void main(String[] args) throws ExecutionException, InterruptedException {
        long l = System.currentTimeMillis();
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {

        });
        executorService.submit(new CallRableThread()).get();
        executorService.submit(new CallRableThread()).get();
        executorService.submit(new CallRableThread()).get();
        executorService.submit(new CallRableThread()).get();
        executorService.shutdown();
        System.out.println(System.currentTimeMillis() - l);
    }
}
