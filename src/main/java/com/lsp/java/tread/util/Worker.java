package com.lsp.java.tread.util;

import java.util.concurrent.CountDownLatch;

/**
 * @Description await()    使当前线程进入同步队列进行等待，直到latch的值被减到0或者当前线程被中断，当前线程就会被唤醒。
 * await(long timeout, TimeUnit unit)	带超时时间的await()。
 * countDown()	使latch的值减1，如果减到了0，则会唤醒所有等待在这个latch上的线程。
 * getCount()	获得latch的数值。
 * @Author lishengpeng
 * @Date 2022/8/2 19:41
 */
public class Worker extends Thread {
    private String name;
    private CountDownLatch c;

    public Worker(String name, CountDownLatch c) {
        this.name = name;
        this.c = c;
    }

    @Override
    public void run() {
        c.countDown();
        System.out.println(this.name + " is running...");
        try {
            Thread.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(this.name + " is end.");
    }
}
