package com.lsp.java.tread.util;

import java.util.concurrent.CountDownLatch;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2022/8/2 19:40
 */
public class WaitThread extends Thread {
    private String name;
    private CountDownLatch c;

    public WaitThread(String name, CountDownLatch c) {
        this.name = name;
        this.c = c;
    }

    @Override
    public void run() {
        try {
            // 等待
            System.out.println(this.name + " wait...");
            c.await();
            System.out.println(this.name + " continue running...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}