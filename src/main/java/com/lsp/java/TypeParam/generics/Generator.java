package com.lsp.java.TypeParam.generics;

public interface Generator<T> {
    T next();
}
