package com.lsp.java.TypeParam.generics;

/**
 * @author lisp
 * @ClassName Coffee
 * @Date 2019/6/6 8:57
 */
public class Coffee {
    private static long counter = 0;
    private final long id = counter++;

    @Override
    public String toString() {
        return getClass().getSimpleName() + "  " + id;
    }
}
