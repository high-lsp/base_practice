package com.lsp.java.TypeParam;

/**
 * @author lisp
 * @ClassName TwoTuple
 * @Date 2019/6/4 12:29
 */
public class TwoTuple<A, B> {
    public final A first;
    public final B second;

    public TwoTuple(A first, B second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public String toString() {
        return "(" + first + "," + second + ")";
    }
}
