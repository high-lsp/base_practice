package com.lsp.java.TypeParam;

/**
 * @author lisp
 * @ClassName LinkedStack
 * @Date 2019/6/5 12:11
 */
public class LinkedStack<T> {

    private static class Node<U> {
        U item;
        Node<U> next;

        Node() {
            item = null;
            next = null;
        }

        Node(U item, Node<U> next) {
            this.item = item;
            this.next = next;
        }

        boolean end() {
            return item == null && next == null;
        }
    }

    private Node<T> top = new Node<T>();

    public void push(T item) {
        top = new Node<T>(item, top);
    }

    public T pop() {
        T result = top.item;
        if (!top.end()) {
            top = top.next;
        }
        return result;
    }

    public static void main(String[] args) {
        LinkedStack<String> lss = new LinkedStack<String>();
        lss.push("stun!");
        lss.push("on");
        lss.push("please");
        System.out.println(lss.pop());
        System.out.println(lss.pop());
        System.out.println(lss.pop());
//        String s;
//        while ((s = lss.pop()) != null) {
//            System.out.println(s);
//        }
    }

}
