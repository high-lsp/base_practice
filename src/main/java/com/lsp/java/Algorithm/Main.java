package com.lsp.java.Algorithm;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/3/9 17:33
 */
public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        //获取总个数
        int total = Integer.valueOf(in.nextLine());
        String[] arrs = new String[total];
        for (int i = 0; i < total; i++) {
            arrs[i] = in.nextLine();
        }
        System.out.println(Arrays.toString(arrs));
//        Arrays.sort(arrs);
//        for (int i = 0; i < arrs.length; i++) {
//            System.out.println(arrs[i]);
//        }

    }

    public static void printNumTail(int num) {
        int a = num;
        int[] r = new int[10];
        while (a > 0) {
            //获取末尾数字
            int tail = a % 10;
            //判断是否打印过了
            if (r[tail] == 0) {
                System.out.print(tail);
                r[tail] = 1;
            }
            a /= 10;
        }
    }

    public static void mergeScan(Scanner sc) {
        while (sc.hasNext()) {
            int count = sc.nextInt();
            Map<Integer, Integer> res = new HashMap<>();
            for (int i = 0; i < count; i++) {
                Integer key = sc.nextInt();
                Integer value = sc.nextInt();
                res.merge(key, value, Integer::sum);
            }
            Object[] objects = res.keySet().toArray();
            Arrays.sort(objects);
            for (Object key : objects) {
                System.out.println(key + " " + res.get((Integer) key));
            }
        }
    }

    public static int count(String source, String target) {
        Map<String, Integer> total = new HashMap<>();
        total.put(target.toLowerCase(), 0);
        for (int i = 0; i < source.length(); i++) {
            System.out.println(String.valueOf(source.charAt(i)).toLowerCase());
            if (total.containsKey(String.valueOf(source.charAt(i)).toLowerCase())) {
                total.put(target, total.get(target) + 1);
            }
        }
        return total.get(target);
    }
}
