package com.lsp.java;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lisp
 * @ClassName MyStatistic
 * @Date 2019/7/10 16:52
 */
public class MyStatistic {
    List<Integer> shuzi = new ArrayList<>();
    List<String> strr = new ArrayList<>();
    List<Long> shuLong = new ArrayList<>();


    public List<Integer> getShuzi() {
        return shuzi;
    }

    public void setShuzi(List<Integer> shuzi) {
        this.shuzi = shuzi;
    }

    public List<String> getStrr() {
        return strr;
    }

    public void setStrr(List<String> strr) {
        this.strr = strr;
    }

    public List<Long> getShuLong() {
        return shuLong;
    }

    public void setShuLong(List<Long> shuLong) {
        this.shuLong = shuLong;
    }
}

