package web.entity;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/21 17:11
 */
@Data
public class QueryParam {
    @NotNull(message = "firstName不能为空")
    private String firstName;
    @NotNull(message = "lastName不能为空")
    private String lastName;
    private String sex;
}
