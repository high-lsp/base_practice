package web.filter;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/23 10:00
 */
@Configuration
@ServletComponentScan
public class FilterConfiguration {
}
