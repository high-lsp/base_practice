package web.exception;

import lombok.Data;

/**
 * @Description 自定义异常
 * @Author lishengpeng
 * @Date 2023/2/21 18:21
 */
@Data
public class BaseException extends RuntimeException {
    private String message;

    public BaseException(String message) {
        super(message);
        this.message = message;
    }

    public BaseException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
    }
}
