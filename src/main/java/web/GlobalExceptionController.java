package web;

import com.alibaba.fastjson.JSONObject;
import com.lsp.java.jvm.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import web.entity.QueryParam;
import web.exception.BaseException;

import javax.validation.Valid;

/**
 * @Description 捕获全局异常
 * @Author lishengpeng
 * @Date 2023/2/21 16:54
 */
@Slf4j
@RestController
public class GlobalExceptionController {

    /**
     * 接口校验,如果有异常则返回校验结果
     *
     * @param queryParam
     * @return java.lang.String
     **/
    @GetMapping("/data")
    public String getTestData(@Valid QueryParam queryParam) {
        log.info("请求参数：{}", JSONObject.toJSONString(queryParam));

        User user = new User();
        user.setId(1);
        user.setName("lisp");
        return JSONObject.toJSONString(user);
    }

    /**
     * 全局异常
     **/
    @GetMapping("/error")
    public void errorMethod() {
        int a = 1 / 0;
    }

    /**
     * 自定义异常
     **/
    @GetMapping("/cuserror")
    public void baseError() {
        throw new BaseException("抛出一个自定义异常");
    }
}
