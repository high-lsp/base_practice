package web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/20 19:29
 */
@RestController
public class HealtyController {

    @GetMapping("/health")
    public String getHealth() {
        return "I`m OK";
    }
}
