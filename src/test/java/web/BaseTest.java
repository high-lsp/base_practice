package web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import web.async.AsyncService;

/**
 * @Description
 * @Author lishengpeng
 * @Date 2023/2/24 12:26
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BaseTest {

    @Autowired
    private AsyncService asyncService;

    @Test
    public void serviceTest() {
        for (int i = 0; i < 10; i++) {
            asyncService.executeAsync();
        }

    }
}
