# base_practice
#### 介绍
日常练手项目


##VUE 2.X 地址
vue学习官网地址：https://v2.cn.vuejs.org/v2/guide/instance.html

##element-ui 地址
element-ui官网地址: https://element.eleme.cn/#/zh-CN/guide/design

##Axios 地址
Axios中文官网： https://www.axios-http.cn/docs/intro

##layui 前端UI框架
layUI官网地址：http://layui.org.cn/index.html

##icons
图标官网地址：https://fontawesome.com/v4/icons/